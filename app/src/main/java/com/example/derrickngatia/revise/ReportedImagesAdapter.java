package com.example.derrickngatia.revise;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

/**
 * Created by DERRICK NGATIA on 6/24/2018.
 */

public class ReportedImagesAdapter extends RecyclerView.Adapter<ReportedImagesAdapter.ViewHolder> {
    Context context;
    ArrayList<report> arrayList;

    public ReportedImagesAdapter(Context context, ArrayList<report> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reported_images, parent, false);

        ReportedImagesAdapter.ViewHolder holder=new ReportedImagesAdapter.ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
    report report=arrayList.get(position);
    holder.description.setText(report.getDescription());
    holder.sender.setText("Posted by: "+ FirebaseAuth.getInstance().getCurrentUser().getEmail());
    holder.date.setText("Posted on: "+report.getImageTime());
        Glide.with(context)
                .load(report.getImageUrl())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        holder.image.setImageBitmap(bitmap);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView sender,description,date;
        ImageView image;
        Button delete,archive;
        public ViewHolder(View itemView) {
            super(itemView);
            sender=(TextView)itemView.findViewById(R.id.sender);
            image=(ImageView)itemView.findViewById(R.id.image);
            description=(TextView)itemView.findViewById(R.id.description);
            date=(TextView)itemView.findViewById(R.id.date);
            delete=(Button)itemView.findViewById(R.id.delete);
            archive=(Button)itemView.findViewById(R.id.archive);


        }

    }
}
