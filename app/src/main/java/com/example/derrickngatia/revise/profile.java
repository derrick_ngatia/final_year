package com.example.derrickngatia.revise;

/**
 * Created by DERRICK NGATIA on 6/24/2018.
 */

public class profile {
    private String name;
    private  String profile_photo_url;

    public profile(String name, String profile_photo_url) {
        this.name = name;
        this.profile_photo_url = profile_photo_url;
    }

    public profile() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_photo_url() {
        return profile_photo_url;
    }

    public void setProfile_photo_url(String profile_photo_url) {
        this.profile_photo_url = profile_photo_url;
    }
}
