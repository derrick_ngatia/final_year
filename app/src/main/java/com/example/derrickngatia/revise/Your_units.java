package com.example.derrickngatia.revise;

import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Your_units extends AppCompatActivity {
    EditText mytext;
    Button submit;
    RecyclerView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_units);
        Intent intent = getIntent();
        final TextView emptyView=(TextView)findViewById(R.id.empty_view);
        final ArrayList<String> listunitcode=new ArrayList<String>();
        listview = (RecyclerView) findViewById(R.id.mlistview);
        final String course = intent.getStringExtra("course");
        final String year = intent.getStringExtra("year");
        final String sem = intent.getStringExtra("sem");
        final ArrayList<String> data=new ArrayList<>();
        data.add(course);
        data.add(year);
        data.add(sem);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference().child("allcourses").child(course).child(year).child(sem);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listunitcode.clear();
                for(DataSnapshot snapshot:dataSnapshot.getChildren()){
                    String unitname=snapshot.getKey();
                    listunitcode.add(unitname);
                }
                FragmentManager fm = getFragmentManager();
                final coursesAdapter1 adapter=new coursesAdapter1(listunitcode,data,getApplicationContext(),fm);
                LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                listview.setLayoutManager(mLayoutManager);
                listview.setAdapter(adapter);
                if (listunitcode.isEmpty()) {
                    listview.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                }
                else {
                    listview.setVisibility(View.VISIBLE);
                    emptyView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /*
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                do {

                    Intent intent=new Intent(Your_units.this,Materials.class);
                    intent.putExtra("storagePath",listview.getItemAtPosition(position).toString());
                    intent.putExtra("course",course);
                    startActivity(intent);

                }while (position==listview.getCount());
            }
        });
*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
