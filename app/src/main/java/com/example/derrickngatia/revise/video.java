package com.example.derrickngatia.revise;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class video extends Fragment {

    RecyclerView recyclerView;
    ProgressBar progressBar;
    static VideosAdapter adapter;
    public video() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view= inflater.inflate(R.layout.fragment_video, container, false);
        final TextView emptyView=(TextView)view.findViewById(R.id.empty_view);
        String query=getArguments().getString("path");
        String my_new_str = query.replace(" ", "+");
        String url="https://www.googleapis.com/youtube/v3/search?part=snippet&q="+my_new_str+"videos&type=video&key=AIzaSyB2TtG6UjEtDWfzxHvQCf2DJxkUPfJI1Lc";
        progressBar=(ProgressBar)view.findViewById(R.id.progress);
        progressBar.setVisibility(View.VISIBLE);
        recyclerView=(RecyclerView)view.findViewById(R.id.recylerViewVideo);
        RequestQueue requestQueue= Volley.newRequestQueue(view.getContext());
        JsonObjectRequest objectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                String jsonString = jsonObject.toString();
                JSONObject jsonResult = null;
                try {
                    jsonResult = new JSONObject(jsonString);
                    JSONArray data = jsonResult.getJSONArray("items");
                    if(data != null) {
                        ArrayList<videos> videos=new ArrayList<>();
                        for (int i=0;i<data.length();i++){
                            JSONObject jObj = data.getJSONObject(i);
                            JSONObject jsonObject1=new JSONObject(jObj.getString("snippet"));
                            JSONObject jsonObject2=new JSONObject(jsonObject1.getString("thumbnails"));
                            JSONObject jsonObject3=new JSONObject(jsonObject2.getString("high"));
                            JSONObject jsonObject4=new JSONObject(jObj.getString("id"));
                            videos.add(new videos(jsonObject1.getString("publishedAt"),jsonObject1.getString("title"),
                                    jsonObject1.getString("description"),jsonObject3.getString("url"),jsonObject4.getString("videoId")));
                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        adapter=new VideosAdapter(videos,getContext());
                        recyclerView.setAdapter(adapter);
                        if (videos.isEmpty()) {
                            recyclerView.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }
                        else {
                            recyclerView.setVisibility(View.VISIBLE);
                            emptyView.setVisibility(View.GONE);
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
        requestQueue.add(objectRequest);
        return  view;
    }
}
