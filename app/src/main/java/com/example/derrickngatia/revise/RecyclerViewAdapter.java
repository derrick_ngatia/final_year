package com.example.derrickngatia.revise;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.*;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import  android.text.format.DateFormat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import android.text.format.DateFormat;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.DOWNLOAD_SERVICE;
import static java.util.Date.*;

/**
 * Created by Derrick Ngatia at 11/12/2012.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> implements Filterable {
    private static ClickListener clickListener;
    DownloadManager downloadManager;
    Bitmap bit;
    Random random;
    Context context;
    List<upload> retrived;
    List<upload> retrivedf;

    String course;

    public RecyclerViewAdapter(Context context, List<upload> retrieved,String course) {

        this.context = context;
        this.course=course;
        this.retrived=retrieved;
        this.retrivedf = retrieved;

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    retrivedf = retrived;
                } else {
                    List<upload> filteredList = new ArrayList<>();
                    for (upload row : retrived) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getImageName().toLowerCase().contains(charString.toLowerCase()) | row.getYear_of_examination().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    retrivedf = filteredList;
                }


                FilterResults filterResults = new FilterResults();
                filterResults.values = retrivedf;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                retrivedf = (List<upload>) filterResults.values;
                notifyDataSetChanged();
            }
        };  }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_items, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Bitmap map;
        String message="uploaded on ";
      final upload image=retrivedf.get(position);
       holder.imageNameTextView.setText(image.getImageName());
       holder.year_of_examination.setText(image.getYear_of_examination());
       holder.imageTime.setText(message+DateFormat.format("yyyy-MM-dd hh:mm:ss a", image.getTimeOfupload()));
        holder.imageView.buildDrawingCache();
        String user=FirebaseAuth.getInstance().getCurrentUser().getEmail();
        int iend = user.indexOf("@");

        String subString = null;
        if (iend != -1)
        {
            subString= user.substring(0 , iend); //this will give abc
        }
        FirebaseDatabase.getInstance().getReference().child("profiles").child(subString.toLowerCase()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               profile profiles=dataSnapshot.getValue(com.example.derrickngatia.revise.profile.class);
                Glide.with(context)
                        .load(profiles.getProfile_photo_url())
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                           holder.profile.setImageBitmap(bitmap);
                            }
                        });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(view.getContext(),moredetails.class);
                intent.putExtra("url",image.getImageURL());
                intent.putExtra("name",image.getImageName());
                view.getContext().startActivity(intent);
            }
        });
        Glide.with(context)
                .load(image.getImageURL())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        bit=bitmap;
                        holder.imageView.setImageBitmap(bitmap);
                    }
                });
        Glide.with(context)
                .load(FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        bit=bitmap;
                        holder.profile.setImageBitmap(bitmap);
                    }
                });
        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                PopupMenu menu=new PopupMenu(view.getContext(),holder.more);
                menu.inflate(R.menu.more);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.report:
                                reportAbuse reportAbuse=new reportAbuse();
                                Bundle bundle=new Bundle();
                                bundle.putString("url",image.getImageURL());
                                bundle.putString("name",image.getImageName());
                                bundle.putString("time",DateFormat.format("yyyy-MM-dd hh:mm:ss a", image.getTimeOfupload()).toString());
                                bundle.putString("course",course);
                                bundle.putString("type","image");
                                reportAbuse.setArguments(bundle);
                                FragmentManager fragmentManager=((Activity)context).getFragmentManager();
                                reportAbuse.show(fragmentManager,"");
                                return true;
                            case R.id.comments:
                                Intent intent=new Intent(view.getContext(),moredetails.class);
                                intent.putExtra("url",image.getImageURL());
                                intent.putExtra("name",image.getImageName());
                                view.getContext().startActivity(intent);
                                return  true;
                            default:
                                return false;
                        }
                    }
                });
                menu.show();
            }
        });
        downloadManager=(DownloadManager)context.getSystemService(DOWNLOAD_SERVICE);

        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(image.getImageURL()));
                view.getContext().startActivity(browserIntent);

            }
        });
    }

    @Override
    public int getItemCount() {

      return retrivedf.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView imageTime;
        public TextView imageNameTextView,year_of_examination;
        public Button download;
        TextView more;
        CircleImageView profile;
        public ViewHolder(View itemView) {
            super(itemView);
            download=(Button)itemView.findViewById(R.id.download);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            year_of_examination=(TextView)itemView.findViewById(R.id.year_of_examination);
            imageTime = (TextView) itemView.findViewById(R.id.time);
            more=(TextView)itemView.findViewById(R.id.more);
            imageNameTextView = (TextView) itemView.findViewById(R.id.ImageNameTextView);
            profile=(CircleImageView)itemView.findViewById(R.id.profile_image);
        }
    }

}