package com.example.derrickngatia.revise;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PdfQuestions extends AppCompatActivity {
    FloatingActionButton button;
    EditText editText;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_questions);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        editText=(EditText)findViewById(R.id.answer);
        button=(FloatingActionButton)findViewById(R.id.fab);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().toString().isEmpty()){
                    editText.setError("Reply required..");
                }else{
                    String id=getIntent().getExtras().getString("date");
                    final String logFileName = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());

                    ChatReply reply=new ChatReply(FirebaseAuth.getInstance().getCurrentUser().getEmail(),editText.getText().toString());
                    FirebaseDatabase.getInstance().getReference().child("questions").child(getIntent().getExtras().getString("date")).push().setValue(reply).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(),"Posted successfully",Toast.LENGTH_SHORT).show();
                            editText.setText("");
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(),"Post failed "+e.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        final ArrayList<ChatReply> arrayList=new ArrayList<>();
        recyclerView=(RecyclerView)findViewById(R.id.recylerView);
        final TextView emptyView=(TextView)findViewById(R.id.empty_view);
        FirebaseDatabase.getInstance().getInstance().getReference().child("questions").child(getIntent().getExtras().getString("date")).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                arrayList.clear();
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    ChatReply reply=snapshot.getValue(ChatReply.class);
                    arrayList.add(reply);
                }
                ReplyAdapter1 adapter=new ReplyAdapter1(arrayList,getApplicationContext());
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                recyclerView.setAdapter(adapter);
                if (arrayList.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                }
                else {
                    recyclerView.setVisibility(View.VISIBLE);
                    emptyView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.questions,menu);
        return super.onCreateOptionsMenu(menu);
    }
}
