package com.example.derrickngatia.revise;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReportedImages extends Fragment {
ArrayList<report> arrayList;
RecyclerView recyclerView;
ProgressDialog dialog;
    public ReportedImages() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view= inflater.inflate(R.layout.fragment_reported_images, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.reported);
        arrayList=new ArrayList<>();
        dialog=new ProgressDialog(view.getContext());
        dialog.setMessage("Retrieving reported images ...");
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        FirebaseDatabase.getInstance().getReference().child("reports").child("images").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                arrayList.clear();
                dialog.show();
               for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                   report report=snapshot.getValue(com.example.derrickngatia.revise.report.class);
                   arrayList.add(report);
               }
               ReportedImagesAdapter adapter=new ReportedImagesAdapter(view.getContext(),arrayList);
                recyclerView.setAdapter(adapter);
                dialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                  dialog.dismiss();
            }
        });
        return view;
    }

}
