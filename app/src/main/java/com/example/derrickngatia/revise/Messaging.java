package com.example.derrickngatia.revise;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by DERRICK NGATIA on 8/12/2018.
 */

public class Messaging extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String tittle=remoteMessage.getNotification().getTitle();
        String message=remoteMessage.getNotification().getBody();

       if (remoteMessage.getNotification().getTitle().equals("New image Item")){
           String course=remoteMessage.getData().get("name");
           String year=remoteMessage.getData().get("url");
           String sem=remoteMessage.getData().get("time");
           String unit=remoteMessage.getData().get("views");
           NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                   .setSmallIcon(R.drawable.graduation)
                   .setContentTitle(tittle)
                   .setContentText(message)
                   .setPriority(NotificationCompat.PRIORITY_DEFAULT);
           int NotifiactionId= (int) System.currentTimeMillis();

           Intent notifyIntent = new Intent(this,Notifications.class);
           notifyIntent.putExtra("name",course);
           notifyIntent.putExtra("url",year);
           notifyIntent.putExtra("time",sem);
           notifyIntent.putExtra("views",unit);
           notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                   | Intent.FLAG_ACTIVITY_CLEAR_TASK);
           PendingIntent notifyPendingIntent = PendingIntent.getActivity(
                   this, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
           );
           mBuilder.setContentIntent(notifyPendingIntent);
           mBuilder.setAutoCancel(true);
           NotificationManager notificationManager=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
           notificationManager.notify(NotifiactionId,mBuilder.build());
       }else if(remoteMessage.getNotification().getTitle().equals("New pdf Item")){
           String course=remoteMessage.getData().get("name");
           String year=remoteMessage.getData().get("url");
           String sem=remoteMessage.getData().get("time");
           String unit=remoteMessage.getData().get("uploader");
           NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                   .setSmallIcon(R.drawable.graduation)
                   .setContentTitle(tittle)
                   .setContentText(message)
                   .setPriority(NotificationCompat.PRIORITY_DEFAULT);
           int NotifiactionId= (int) System.currentTimeMillis();

           Intent notifyIntent = new Intent(this,pdf_Notifications.class);
           notifyIntent.putExtra("name",course);
           notifyIntent.putExtra("url",year);
           notifyIntent.putExtra("time",sem);
           notifyIntent.putExtra("views",unit);
           notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                   | Intent.FLAG_ACTIVITY_CLEAR_TASK);
           PendingIntent notifyPendingIntent = PendingIntent.getActivity(
                   this, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
           );
           mBuilder.setContentIntent(notifyPendingIntent);
           mBuilder.setAutoCancel(true);
           NotificationManager notificationManager=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
           notificationManager.notify(NotifiactionId,mBuilder.build());
       }else if (remoteMessage.getNotification().getTitle().equals("Revise Subscriptions")){
           NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                   .setSmallIcon(R.drawable.graduation)
                   .setContentTitle(tittle)
                   .setContentText(message)
                   .setPriority(NotificationCompat.PRIORITY_DEFAULT);
           int NotifiactionId= (int) System.currentTimeMillis();
           mBuilder.setAutoCancel(true);
           NotificationManager notificationManager=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
           notificationManager.notify(NotifiactionId,mBuilder.build());


       }


    }

}
