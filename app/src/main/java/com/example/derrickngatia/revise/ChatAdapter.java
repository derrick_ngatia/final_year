package com.example.derrickngatia.revise;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.text.format.DateFormat;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DERRICK NGATIA on 6/30/2018.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder>  implements  Filterable{
    ArrayList<ChatQuestion> arrayList;
    View view;
    List<ChatQuestion> arrayListf;
    Context context;

    public ChatAdapter(ArrayList<ChatQuestion> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        this.arrayListf=arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatadapter, parent, false);

        ChatAdapter.ViewHolder holder = new ChatAdapter.ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
       final ChatQuestion question=arrayListf.get(position);
        final String user= question.getSender();
        final int iend = user.indexOf("@");
        String subString = null;
        if (iend != -1)
        {
            subString= user.substring(0 , iend); //this will give abc
        }
       holder.sender.setText("@"+subString);
        holder.time.setText(DateFormat.format("yyyy-MM-dd hh:mm:ss a", question.getDate()));
        holder.message.setText(question.getMessage());
        holder.tag.setText("#Question "+question.getTag());
        holder.reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent=new Intent(context,Replies.class);
               intent.putExtra("sender",question.getSender());
                intent.putExtra("message",question.getMessage());
                intent.putExtra("date",DateFormat.format("yyyy-MM-dd hh:mm:ss a", question.getDate()));
                intent.putExtra("tag",question.getTag());
                intent.putExtra("id",question.getDate().toString());
               view.getContext().startActivity(intent);
            }
        });
        FirebaseDatabase.getInstance().getReference().child("replies").child(question.getDate().toString()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount()==1){
                    holder.top.setText(dataSnapshot.getChildrenCount()+" reply");

                }
                holder.top.setText(dataSnapshot.getChildrenCount()+" replies");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayListf.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    arrayListf = arrayList;
                } else {
                    List<ChatQuestion> filteredList = new ArrayList<>();
                    for ( ChatQuestion row : arrayList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTag().toLowerCase().contains(charString.toLowerCase()) |
                                row.getMessage().toLowerCase().contains(charString.toLowerCase()) |
                                row.getSender().toLowerCase().contains(charString.toLowerCase()) |
                                DateFormat.format("yyyy-MM-dd hh:mm:ss a", row.getDate()).toString().toLowerCase().contains(charString.toLowerCase())

                                ) {
                            filteredList.add(row);
                        }
                    }

                    arrayListf = filteredList;
                }


                FilterResults filterResults = new FilterResults();
                filterResults.values = arrayListf;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                arrayListf = (List<ChatQuestion>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
       public TextView sender,time,message,top,tag,reply;
       CardView cardView;
        public ViewHolder(View itemView) {
            super(itemView);
            tag=(TextView)itemView.findViewById(R.id.tag);
            sender=(TextView)itemView.findViewById(R.id.sender);
            time=(TextView)itemView.findViewById(R.id.time);
            message=(TextView)itemView.findViewById(R.id.message);
            top=(TextView)itemView.findViewById(R.id.top);
            reply=(TextView)itemView.findViewById(R.id.reply);
            cardView=(CardView)itemView.findViewById(R.id.card);

        }

    }
}
