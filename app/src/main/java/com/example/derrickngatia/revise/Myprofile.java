package com.example.derrickngatia.revise;


import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class Myprofile extends DialogFragment {
    private static final int IMAGE_PICK_REQUEST = 123;
    CircleImageView profile;
TextView email;
    profile profiles;
Button edit;
ProgressDialog dialog;
    private Uri filepath;

    public Myprofile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =inflater.inflate(R.layout.fragment_myprofile, container, false);
        profile=(CircleImageView)view.findViewById(R.id.profile_image);
        email=(TextView)view.findViewById(R.id.email);
        edit=(Button) view.findViewById(R.id.edit);
        dialog=new ProgressDialog(view.getContext());
        dialog.setTitle("...  " +
                "+");
        dialog.setMessage("updating profile image...");
        String user=FirebaseAuth.getInstance().getCurrentUser().getEmail();
        int iend = user.indexOf("@");

        String subString = null;
        if (iend != -1)
        {
            subString= user.substring(0 , iend); //this will give abc
        }
        FirebaseDatabase.getInstance().getReference().child("profiles").child(subString.toLowerCase()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                profiles=dataSnapshot.getValue(com.example.derrickngatia.revise.profile.class);
                Glide.with(view.getContext())
                        .load(profiles.getProfile_photo_url())
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                profile.setImageBitmap(bitmap);
                            }
                        });
                email.setText(profiles.getName());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent,"select an image"),IMAGE_PICK_REQUEST);
            }
        });

        return  view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==IMAGE_PICK_REQUEST && resultCode==RESULT_OK && data!=null && data.getData()!=null){

            filepath=data.getData();
            try {
                Bitmap map= MediaStore.Images.Media.getBitmap(getContext().getContentResolver(),filepath);
                profile.setImageBitmap(map);
                final String logFileName = new SimpleDateFormat("yyyyMMddHHmm'.pdf'").format(new Date());
                dialog.show();
                dialog.setCancelable(true);
                FirebaseStorage.getInstance().getReference().child("profiles/"+logFileName).putFile(filepath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        profile profile=new profile(profiles.getName(),taskSnapshot.getDownloadUrl().toString());
                        String user=FirebaseAuth.getInstance().getCurrentUser().getEmail();
                        int iend = user.indexOf("@");

                        String subString = null;
                        if (iend != -1)
                        {
                            subString= user.substring(0 , iend); //this will give abc
                        }

                        FirebaseDatabase.getInstance().getReference().child("profiles").child(subString).setValue(profile).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                             dialog.dismiss();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getContext(),""+e.getMessage(),Toast.LENGTH_LONG).show();
                             dialog.dismiss();
                            }
                        });

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });



            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
