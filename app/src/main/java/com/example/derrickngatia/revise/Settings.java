package com.example.derrickngatia.revise;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Settings extends android.app.Fragment {
CircleImageView imageView;
profile profiles;
TextView name,email;

    public Settings() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view= inflater.inflate(R.layout.fragment_settings, container, false);
        imageView=(CircleImageView)view.findViewById(R.id.image);
        email=(TextView)view.findViewById(R.id.email);
        email.setText("Email: " +FirebaseAuth.getInstance().getCurrentUser().getEmail());
        name=(TextView)view.findViewById(R.id.name);
        String user=FirebaseAuth.getInstance().getCurrentUser().getEmail();
        int iend = user.indexOf("@");

        String subString = null;
        if (iend != -1)
        {
            subString= user.substring(0 , iend); //this will give abc
        }
        FirebaseDatabase.getInstance().getReference().child("profiles").child(subString.toLowerCase()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                profiles=dataSnapshot.getValue(com.example.derrickngatia.revise.profile.class);
                Glide.with(view.getContext())
                        .load(profiles.getProfile_photo_url())
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                imageView.setImageBitmap(bitmap);
                            }
                        });
                name.setText("Name:"+profiles.getName());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return  view;
    }

}
