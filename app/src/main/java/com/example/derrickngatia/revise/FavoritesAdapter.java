package com.example.derrickngatia.revise;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

/**
 * Created by DERRICK NGATIA on 8/14/2018.
 */

public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.ViewHolder> {
    ArrayList<String> favorites;
    Context context;

    public FavoritesAdapter(ArrayList<String> favorites, Context context) {
        this.favorites = favorites;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fav, parent, false);

        FavoritesAdapter.ViewHolder viewHolder = new FavoritesAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
          holder.textView.setText(favorites.get(position));
          holder.button.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  FirebaseDatabase.getInstance().getReference("Notifications").child(favorites.get(position)).child(FirebaseInstanceId.getInstance().getToken()).addValueEventListener(new ValueEventListener() {
                      @Override
                      public void onDataChange(DataSnapshot dataSnapshot) {
                                  dataSnapshot.getRef().removeValue();
                                  Toast.makeText(context,"deleted",Toast.LENGTH_SHORT).show();


                      }

                      @Override
                      public void onCancelled(DatabaseError databaseError) {

                      }
                  });
              }
          });
    }

    @Override
    public int getItemCount() {
        return favorites.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageButton button;
        public ViewHolder(View itemView) {
            super(itemView);
          button=(ImageButton)itemView.findViewById(R.id.delete);
          textView=(TextView)itemView.findViewById(R.id.unit);


        }

    }
}
