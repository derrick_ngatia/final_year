package com.example.derrickngatia.revise;

/**
 * Created by DERRICK NGATIA on 6/29/2018.
 */

public class gcourses {
    private  String course_name;

    public gcourses(String course_name) {
        this.course_name = course_name;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }
}
