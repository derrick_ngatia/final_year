package com.example.derrickngatia.revise;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class sem extends Fragment {
  Button sem1,sem2;

    public sem() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final String course=getArguments().getString("course");
        final String year=getArguments().getString("year");
        View view= inflater.inflate(R.layout.fragment_sem, container, false);
        sem1=(Button)view.findViewById(R.id.sem1);
        sem2=(Button) view.findViewById(R.id.sem2);
        sem1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                units s=new units();
                Bundle b1=new Bundle();
                b1.putString("course",course);
                b1.putString("year",year);
                b1.putString("sem","1");
                s.setArguments(b1);
                getFragmentManager().beginTransaction().replace(R.id.container89,s).addToBackStack("").commit();
            }
        });
        sem2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                units s=new units();
                Bundle b1=new Bundle();
                b1.putString("course",course);
                b1.putString("year",year);
                b1.putString("sem","2");
                s.setArguments(b1);
                getFragmentManager().beginTransaction().replace(R.id.container89,s).addToBackStack("").commit();
            }
        });
        return view;
    }

}
