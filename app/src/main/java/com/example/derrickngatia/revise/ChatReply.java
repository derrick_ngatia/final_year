package com.example.derrickngatia.revise;

import java.util.Date;

/**
 * Created by DERRICK NGATIA on 6/30/2018.
 */

public class ChatReply {
    private String sender;
    private String message;
    private Long Date;
    private String thumbsUp;
    private  String thumbsDown;

    public ChatReply() {
    }

    public ChatReply(String sender, String message) {
        this.sender = sender;
        this.message = message;
        this.Date= new Date().getTime();
        this.thumbsUp="0";
        this.thumbsDown="0";
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getDate() {
        return Date;
    }

    public void setDate(Long date) {
        Date = date;
    }

    public String getThumbsUp() {
        return thumbsUp;
    }

    public void setThumbsUp(String thumbsUp) {
        this.thumbsUp = thumbsUp;
    }

    public String getThumbsDown() {
        return thumbsDown;
    }

    public void setThumbsDown(String thumbsDown) {
        this.thumbsDown = thumbsDown;
    }
}
