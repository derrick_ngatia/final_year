package com.example.derrickngatia.revise;


import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fback extends DialogFragment {
Button send;
EditText description;
RadioButton bug,other;
ProgressDialog dialog;


    public Fback() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_fback, container, false);
        send=(Button)view.findViewById(R.id.send);
        description=(EditText)view.findViewById(R.id.description);
        bug=(RadioButton) view.findViewById(R.id.bug);
        other=(RadioButton) view.findViewById(R.id.other);
        dialog=new ProgressDialog(view.getContext());
        dialog.setMessage("sending ....");
        dialog.setCancelable(false);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (description.getText().toString().isEmpty()){
                    description.setError("Please add a message for the feedback");
                }
                else {
                    dialog.show();
                    if (bug.isChecked()){
                        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
                        ResponseFeedBack feedBack=new ResponseFeedBack(FirebaseAuth.getInstance().getCurrentUser().getEmail().toString(),
                                description.getText().toString(),timeStamp,"bug",0);
                        FirebaseDatabase.getInstance().getReference().child("feedback").push().setValue(feedBack).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                dialog.dismiss();
                                dismiss();
                                Snackbar.make(view,"Successfully sent feedback",Snackbar.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                dialog.dismiss();
                                Snackbar.make(view,"Error, "+e.getMessage(),Snackbar.LENGTH_SHORT).show();
                            }
                        });

                    }else if (other.isChecked()){
                        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
                        ResponseFeedBack feedBack=new ResponseFeedBack(FirebaseAuth.getInstance().getCurrentUser().getEmail().toString(),
                                description.getText().toString(),timeStamp,"other",0);
                        FirebaseDatabase.getInstance().getReference().child("feedback").push().setValue(feedBack).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                dialog.dismiss();
                                dismiss();
                                Snackbar.make(view,"Successfully sent feedback",Snackbar.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                dialog.dismiss();
                                Snackbar.make(view,"Error, "+e.getMessage(),Snackbar.LENGTH_SHORT).show();
                            }
                        });

                    }else {
                        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
                        ResponseFeedBack feedBack=new ResponseFeedBack(FirebaseAuth.getInstance().getCurrentUser().getEmail().toString(),
                                description.getText().toString(),timeStamp,"Not Available",0);
                        FirebaseDatabase.getInstance().getReference().child("feedback").push().setValue(feedBack).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                dialog.dismiss();
                                dismiss();
                                Toast.makeText(view.getContext(),"Feedback sent Successfully",Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                dialog.dismiss();
                                Toast.makeText(view.getContext(),"Error, "+e.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                }
            }
        });
        return view;
    }

}
