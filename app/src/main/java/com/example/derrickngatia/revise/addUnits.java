package com.example.derrickngatia.revise;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class addUnits extends Fragment {
Spinner course,year,sem;
dbhelper helper;
EditText text;
Button button;
ArrayList<String> arrayList,asArray;
    public addUnits() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_add_units, container, false);
        course=(Spinner) view.findViewById(R.id.course);
        helper=new dbhelper(getActivity());
        year=(Spinner)view.findViewById(R.id.year);
        sem=(Spinner)view.findViewById(R.id.sem);
        button=(Button)view.findViewById(R.id.ongeza);
        text=(EditText)view.findViewById(R.id.unit);
        arrayList=new ArrayList<>();
        asArray=new ArrayList<>();
        asArray.add("1");
        asArray.add("2");
        arrayList.add("1");
        arrayList.add("2");
        arrayList.add("3");
        arrayList.add("4");
        helper=new dbhelper(getActivity());
        ArrayAdapter<String> adapter2=new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,asArray);
        ArrayAdapter<String> adapter1=new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,arrayList);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,helper.getCourse());
        course.setAdapter(adapter);
        year.setAdapter(adapter1);
        sem.setAdapter(adapter2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cm=course.getSelectedItem().toString();
                String id=helper.getCourseId(cm);
                Boolean in=helper.insertin(Integer.parseInt(id),text.getText().toString(),Integer.parseInt(year.getSelectedItem().toString()),Integer.parseInt(sem.getSelectedItem().toString()));
            if(in==true){
                Toast.makeText(getActivity(),"inserted",Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getActivity(),"failed",Toast.LENGTH_SHORT).show();
            }

            }
        });
        return view;
    }

}
