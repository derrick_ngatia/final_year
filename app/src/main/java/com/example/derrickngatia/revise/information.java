package com.example.derrickngatia.revise;

/**
 * Created by DERRICK NGATIA on 10/10/2017.
 */

public class information {
    String username;
    String emailVerified;
    String email;
    String password;

    public information(String username, String emailVerified, String email, String password) {
        this.username = username;
        this.emailVerified = emailVerified;
        this.email = email;
        this.password = password;
    }

    public information() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(String emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
