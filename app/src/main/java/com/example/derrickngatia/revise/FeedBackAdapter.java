package com.example.derrickngatia.revise;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by DERRICK NGATIA on 6/23/2018.
 */

public class FeedBackAdapter extends  RecyclerView.Adapter<FeedBackAdapter.ViewHolder> {
    ArrayList<ResponseFeedBack> arrayList;
    Context context;
    private static final Random RANDOM = new Random();
    int[] mycolors=
            {
                    R.color.AliceBlue,R.color.AntiqueWhite,R.color.Aqua,R.color.Bisque,R.color.Fuchsia,R.color.ForestGreen,R.color.BlanchedAlmond,R.color.GreenYellow,R.color.Indigo,R.color.Maroon,R.color.PaleVioletRed,R.color.Purple
            };
    public FeedBackAdapter(ArrayList<ResponseFeedBack> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    public FeedBackAdapter() {
    }

    @Override
    public FeedBackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feedback, parent, false);

        FeedBackAdapter.ViewHolder holder=new FeedBackAdapter.ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final FeedBackAdapter.ViewHolder holder, int position) {
      final ResponseFeedBack feedBack=arrayList.get(position);
      if (feedBack.getRead()==2){
          holder.delete.setEnabled(false);
      }
        holder.icon.setInitials(true);
        holder.icon.setLetterColor(R.color.Fuchsia);
        holder.icon.setLetterSize(18);
        holder.icon.setShapeColor(mycolors[RANDOM.nextInt(mycolors.length)]);
        holder.icon.setShapeType(MaterialLetterIcon.Shape.CIRCLE);
        holder.icon.setLetterTypeface(Typeface.SANS_SERIF);
        holder.icon.setLetter(feedBack.getSender());
        holder.sender.setText("@"+feedBack.getSender());
        holder.description.setText(feedBack.getMessage());
        holder.date.setText("Uploaded on "+feedBack.getTime());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                FirebaseDatabase.getInstance().getReference().child("feedback").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                            ResponseFeedBack feedBack1=snapshot.getValue(ResponseFeedBack.class);
                            if (feedBack1.getTime().equals(feedBack.getTime())){
                                snapshot.getRef().removeValue();
                                Toast.makeText(view.getContext(),"Deleted",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
        holder.archive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (feedBack.getRead() == 2) {
                    holder.archive.setText("Archived");
                    Toast.makeText(view.getContext(), "Already Archived.. To delete Long Press...", Toast.LENGTH_SHORT).show();
                } else {
                    FirebaseDatabase.getInstance().getReference().child("feedback").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                ResponseFeedBack feedBack1 = snapshot.getValue(ResponseFeedBack.class);
                                if (feedBack1.getTime().equals(feedBack.getTime())) {
                                    ResponseFeedBack feedBack2 = new ResponseFeedBack(feedBack1.getSender(), feedBack1.getMessage()
                                            , feedBack1.getTime(), feedBack1.getCategory(), 2);
                                    snapshot.getRef().setValue(feedBack2);
                                    Toast.makeText(view.getContext(), "Archived", Toast.LENGTH_SHORT).show();
                                    holder.delete.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cannot,0,0,0);
                                    holder.archive.setCompoundDrawablesWithIntrinsicBounds(R.drawable.archive,0,0,0);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }
        });
        holder.archive.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                holder.delete.setEnabled(true);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public MaterialLetterIcon icon;
        TextView sender,description,date;
        Button delete,archive;
        public ViewHolder(View itemView) {
            super(itemView);
            icon=(MaterialLetterIcon)itemView.findViewById(R.id.icon);
            sender=(TextView)itemView.findViewById(R.id.sender);
            description=(TextView)itemView.findViewById(R.id.description);
            date=(TextView)itemView.findViewById(R.id.date);
            delete=(Button)itemView.findViewById(R.id.delete);
            archive=(Button)itemView.findViewById(R.id.archive);


        }

    }
}
