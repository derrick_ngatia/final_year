package com.example.derrickngatia.revise;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class add extends Fragment {
EditText coursename,courseid,unitname,year,sem;
Button submit;
dbhelper helper;

    public add() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_add, container, false);
        helper=new dbhelper(getActivity());
        coursename=(EditText)view.findViewById(R.id.coursename);
        courseid=(EditText)view.findViewById(R.id.courseid);
        unitname=(EditText)view.findViewById(R.id.unitname);
        year=(EditText)view.findViewById(R.id.year);
        sem=(EditText)view.findViewById(R.id.sem);
        submit=(Button)view.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Boolean is= helper.add(Integer.parseInt(courseid.getText().toString()),coursename.getText().toString(),unitname.getText().toString(),Integer.parseInt(year.getText().toString()),Integer.parseInt(sem.getText().toString()));
            if(is==true){
                Toast.makeText(getActivity(),"success",Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getActivity(),"fail",Toast.LENGTH_SHORT).show();
            }
            }
        });
        return view;
    }

}
