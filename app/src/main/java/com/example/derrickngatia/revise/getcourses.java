package com.example.derrickngatia.revise;

import android.*;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SearchEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.io.IOException;

import static com.example.derrickngatia.revise.R.id.about;
import static com.example.derrickngatia.revise.R.id.exitAccount;
import static com.example.derrickngatia.revise.R.id.profile;
import static com.example.derrickngatia.revise.R.id.share;
//import static com.example.derrickngatia.revise.R.id.uploading;
import static com.example.derrickngatia.revise.R.menu.signout;

public class getcourses extends AppCompatActivity {
    dbhelper helper;
    EditText text;
    BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        helper=new dbhelper(this);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        getThem semester = new getThem();
        fragmentTransaction.replace(R.id.container, semester);
        fragmentTransaction.addToBackStack("").commit();
        bottomNavigationView= (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home:
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        getThem semester = new getThem();
                        fragmentTransaction.replace(R.id.container, semester);
                        fragmentTransaction.addToBackStack("").commit();
                        break;
                    case R.id.favorite:
                        favorites fav=new favorites();
                        getFragmentManager().beginTransaction().replace(R.id.container,fav).addToBackStack("").commit();
                        break;
                    case R.id.settings:
                        Settings settings=new Settings();
                        getFragmentManager().beginTransaction().replace(R.id.container,settings).addToBackStack("").commit();
                        break;
                }
                return true;
            }
        });

    }

    @Override
    public void onBackPressed() {
        //EditText search=(EditText)findViewById(R.id.search);
        //search.setVisibility(View.INVISIBLE);
        new AlertDialog.Builder(this).setIcon(R.drawable.exit).setTitle("Exiting application").setMessage("Are you sure you want to exit Qeazy?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        }).setNeutralButton("Dont Remind me", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }



}

