// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'

Vue.config.productionTip = false


// Initialize Firebase
let app;
var config = {
    apiKey: "AIzaSyA4a4hZq6hRQ5uZThG8pQu2ATjktuHZnMI",
    authDomain: "school-project-e1dd0.firebaseapp.com",
    databaseURL: "https://school-project-e1dd0.firebaseio.com",
    projectId: "school-project-e1dd0",
    storageBucket: "school-project-e1dd0.appspot.com",
    messagingSenderId: "702656228880"
};
firebase.initializeApp(config);

/* eslint-disable no-new */
firebase.auth().onAuthStateChanged(function (user) {
    if (!app){
        new Vue({
            el: '#app',
            router,
            components: { App },
            template: '<App/>'
        })
    }
    
})

