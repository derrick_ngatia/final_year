package com.example.derrickngatia.revise;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.dd.morphingbutton.MorphingButton;

import java.util.ArrayList;

public class yearandsem extends AppCompatActivity {
    String course;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yearandsem);
        Intent intent=getIntent();
        course=intent.getStringExtra("course");
        year y=new year();
        Bundle bundle=new Bundle();
        bundle.putString("course",course);
        y.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container,y).addToBackStack("").commit();
    }
   /* private void morphToFailure(final MorphingButton btnMorph, int duration) {
        MorphingButton.Params circle = MorphingButton.Params.create()
                .duration(duration)
                .cornerRadius(dimen(R.dimen.mb_height_56))
                .width(dimen(R.dimen.mb_height_56))
                .height(dimen(R.dimen.mb_height_56))
                .color(color(R.color.mb_red))
                .colorPressed(color(R.color.mb_red_dark))
                .icon(R.drawable.ic_lock);
        btnMorph.morph(circle);
    }
    */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
