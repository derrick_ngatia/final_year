package com.example.derrickngatia.revise;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ivbaranov.mli.MaterialLetterIcon;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 * Created by DERRICK NGATIA on 3/17/2018.
 */

public class coursesAdapter extends RecyclerView.Adapter<coursesAdapter.ViewHolder> implements Filterable {
    Random RANDOM = new Random();
    List<gcourses> course;
    List<gcourses> filtered;
    Context context;
    int[] mycolors =
            {
                    R.color.AliceBlue, R.color.AntiqueWhite, R.color.Aqua, R.color.Bisque, R.color.Fuchsia, R.color.ForestGreen, R.color.BlanchedAlmond, R.color.GreenYellow, R.color.Indigo, R.color.Maroon, R.color.PaleVioletRed, R.color.Purple
            };

    public coursesAdapter(List<gcourses> course, Context context) {
        this.course = course;
        this.filtered = course;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlist_item, parent, false);

        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        gcourses gcourses = filtered.get(position);
        final String name = gcourses.getCourse_name();
        holder.imageNameTextView.setText("  " + name);
        holder.imageView.setInitials(true);
        holder.imageView.setLetterColor(R.color.Fuchsia);
        holder.imageView.setLetterSize(18);
        holder.imageView.setShapeColor(mycolors[RANDOM.nextInt(mycolors.length)]);
        holder.imageView.setShapeType(MaterialLetterIcon.Shape.ROUND_RECT);
        holder.imageView.setLetterTypeface(Typeface.SANS_SERIF);
        holder.imageView.setLetter(name);
        holder.imageView.setRoundRectRx(8); // defaults x-corner radius
        holder.imageView.setRoundRectRy(8);
        holder.imageNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(name.toLowerCase().contains("engineering")){
                    Intent intent = new Intent(view.getContext(), engineering.class);
                    intent.putExtra("course", name);
                    view.getContext().startActivity(intent);
                }else if(name.toLowerCase().contains("medicine")){
                    Intent intent = new Intent(view.getContext(), medicine.class);
                    intent.putExtra("course", name);
                    view.getContext().startActivity(intent);

                }else{
                    Intent intent = new Intent(view.getContext(), yearandsem.class);
                    intent.putExtra("course", name);
                    view.getContext().startActivity(intent);
                }

            }
        });


    }


    @Override
    public int getItemCount() {
        return filtered.size();

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filtered = course;
                } else {
                    List<gcourses> filteredList = new ArrayList<>();
                    for (gcourses row : course) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCourse_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filtered = filteredList;
                }


                FilterResults filterResults = new FilterResults();
                filterResults.values = filtered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filtered = (List<gcourses>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }



    static class ViewHolder extends RecyclerView.ViewHolder {
    public  TextView imageNameTextView;
    public MaterialLetterIcon imageView;
    public Button download;
    public ViewHolder(View itemView) {
        super(itemView);
        download=(Button)itemView.findViewById(R.id.download);
        imageView = (MaterialLetterIcon) itemView.findViewById(R.id.listIcon);
        imageNameTextView = (TextView) itemView.findViewById(R.id.courses);
    }

}
}