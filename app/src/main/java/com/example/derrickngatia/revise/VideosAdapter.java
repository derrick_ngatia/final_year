package com.example.derrickngatia.revise;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DERRICK NGATIA on 6/23/2018.
 */

public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.ViewHolder> implements Filterable {
    ArrayList<videos> videos;
    List<videos> videosF;
    Context context;

    public VideosAdapter() {
    }

    public VideosAdapter(ArrayList<com.example.derrickngatia.revise.videos> videos, Context context) {
        this.videos = videos;
        this.context = context;
        this.videosF=videos;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final videos videos1=videosF.get(position);
        Glide.with(context)
                .load(videos1.getThumbNails())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        holder.imageView.setImageBitmap(bitmap);
                    }
                });
        holder.published_at.setText("Published on "+videos1.getPublishedAt());
        holder.description.setText("Description: "+videos1.getDescription());
        holder.tittle.setText("Title: "+videos1.getTittle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(view.getContext(), VideoActivity.class);
                intent.putExtra("videoId",videos1.getVideoId());
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return videosF.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    videosF = videos;
                } else {
                    List<videos> filteredList = new ArrayList<>();
                    for (videos row :videosF ) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTittle().toLowerCase().contains(charString.toLowerCase()) | row
                                .getDescription().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    videosF= filteredList;
                }


                FilterResults filterResults = new FilterResults();
                filterResults.values = videosF;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                videosF = (List<videos>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView description,tittle,published_at;
        public ViewHolder(View itemView) {
            super(itemView);
            description=(TextView)itemView.findViewById(R.id.description);
            tittle=(TextView)itemView.findViewById(R.id.title);
            published_at=(TextView)itemView.findViewById(R.id.published_at);
            imageView=(ImageView)itemView.findViewById(R.id.snippet);

        }
    }
}
