package com.example.derrickngatia.revise;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.util.ArrayList;

public class addmaterials extends Activity {
Spinner spn1,spn2,spn3;
ArrayList<String> yas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addmaterials);
        yas=new ArrayList<>();
        yas.add("Year 1 Semester 1");
        yas.add("Year 1 Semester 2");
        yas.add("Year 2 Semester 1");
        yas.add("Year 2 Semester 2");
        yas.add("Year 3 Semester 1");
        yas.add("Year 3 Semester 2");
        yas.add("Year 4 Semester 1");
        yas.add("Year 4 Semester 2");
        spn1=(Spinner)findViewById(R.id.sp1);
        spn2=(Spinner)findViewById(R.id.sp2);
        spn3=(Spinner)findViewById(R.id.sp3);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(getApplicationContext(),R.layout.support_simple_spinner_dropdown_item,yas);
        spn2.setAdapter((SpinnerAdapter) adapter);
    }
}
