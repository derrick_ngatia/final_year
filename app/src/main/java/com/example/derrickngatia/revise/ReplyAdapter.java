package com.example.derrickngatia.revise;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.sql.Date;
import android.text.format.DateFormat;
import java.util.ArrayList;

/**
 * Created by DERRICK NGATIA on 7/4/2018.
 */

public class ReplyAdapter extends RecyclerView.Adapter<ReplyAdapter.ViewHolder> {
    ArrayList<ChatReply> reply;
    Context context;

    public ReplyAdapter(ArrayList<ChatReply> reply, Context context) {
        this.reply = reply;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.answer, parent, false);

        ReplyAdapter.ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ChatReply replys=reply.get(position);
        holder.message.setText(replys.getMessage());
        holder.time.setText(DateFormat.format("yyyy-MM-dd hh:mm:ss a", replys.getDate()));
        final String user= replys.getSender();
        final int iend = user.indexOf("@");
        String subString = null;
        if (iend != -1)
        {
            subString= user.substring(0 , iend); //this will give abc
        }
        holder.sender.setText("@"+subString);
    }

    @Override
    public int getItemCount() {
        return reply.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView sender,time,message;
        CardView cardView;
        public ViewHolder(View itemView) {
            super(itemView);
            time=(TextView)itemView.findViewById(R.id.time);
            message=(TextView)itemView.findViewById(R.id.message);
            sender=(TextView)itemView.findViewById(R.id.sender) ;
            cardView=(CardView)itemView.findViewById(R.id.card);

        }

    }
}
