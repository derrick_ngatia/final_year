package com.example.derrickngatia.revise;

import java.util.Date;

/**
 * Created by DERRICK NGATIA on 6/30/2018.
 */

public class ChatQuestion {
    private String message;
    private Long Date;
    private  String sender;
    private  String tag;
    private  String topReply;
    private boolean read;

    public ChatQuestion() {
    }

    public ChatQuestion(String message, String sender, String tag, String topReply) {
        this.message = message;
        Date = new Date().getTime();
        this.sender = sender;
        this.tag = tag;
        this.topReply = topReply;
        this.read = false;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getDate() {
        return Date;
    }

    public void setDate(Long date) {
        Date = date;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTopReply() {
        return topReply;
    }

    public void setTopReply(String topReply) {
        this.topReply = topReply;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }
}
