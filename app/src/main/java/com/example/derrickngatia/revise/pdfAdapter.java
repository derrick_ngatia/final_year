package com.example.derrickngatia.revise;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.firebase.auth.FirebaseAuth;

import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Derrick Ngatia at 11/12/2012.
 */

public class pdfAdapter extends RecyclerView.Adapter<pdfAdapter.ViewHolder> implements Filterable {
    ArrayList<document> arrayList;
    List<document> arrayListf;
    Context context;

    public pdfAdapter(ArrayList<document> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        this.arrayListf=arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pdfview, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final document document=arrayListf.get(position);
        holder.imageTime.setText("uploaded on "+document.getTime());
        holder.imageNameTextView.setText("Tittle: "+document.getName());
        try {
            String urlEncoded = URLEncoder.encode(document.getUrl(), "UTF-8");
            holder.pdfView.getSettings().setJavaScriptEnabled(true);
            holder.pdfView.getSettings().setPluginState(WebSettings.PluginState.ON);
            holder.pdfView.loadUrl("http://docs.google.com/viewer?url=" +urlEncoded);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        holder.pdfView.setClickable(false);
        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu menu=new PopupMenu(view.getContext(),holder.more);
                menu.inflate(R.menu.more);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.report:
                                reportAbuse reportAbuse=new reportAbuse();
                                Bundle bundle=new Bundle();
                                bundle.putString("url",document.getUrl());
                                bundle.putString("name",document.getName());
                                bundle.putString("time",document.getName());
                                bundle.putString("type","pdf");
                                reportAbuse.setArguments(bundle);
                                FragmentManager fragmentManager=((Activity)context).getFragmentManager();
                                reportAbuse.show(fragmentManager,"");
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                menu.show();
            }
        });
        holder.year_of_examination.setText(document.getYear_of_examination());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context,PdfPreview.class);
                i.putExtra("url",document.getUrl());
                view.getContext().startActivity(i);
            }
        });
        holder.comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,PdfQuestions.class);
                intent.putExtra("date", document.getTime());
                view.getContext().startActivity(intent);
            }
        });
  holder.download.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
          Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(document.getUrl()));
          view.getContext().startActivity(browserIntent);

      }
  });

    }

    @Override
    public int getItemCount() {

      return arrayListf.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    arrayListf = arrayList;
                } else {
                    List<document> filteredList = new ArrayList<>();
                    for (document row : arrayList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) | row
                                .getTime().contains(charString.toLowerCase()) | row.getYear_of_examination().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    arrayListf= filteredList;
                }


                FilterResults filterResults = new FilterResults();
                filterResults.values = arrayListf;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                arrayListf = (List<document>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        public WebView pdfView;
        public TextView imageTime;
        public TextView imageNameTextView,year_of_examination;
        public TextView more;
        public Button download;
        public TextView comments;
        public ViewHolder(View itemView) {
            super(itemView);
            year_of_examination=(TextView)itemView.findViewById(R.id.year_of_examination) ;
            pdfView = (WebView) itemView.findViewById(R.id.pdf);
            imageTime = (TextView) itemView.findViewById(R.id.time);
            imageNameTextView = (TextView) itemView.findViewById(R.id.Name);
            more=(TextView)itemView.findViewById(R.id.more);
            download=(Button)itemView.findViewById(R.id.download);
            comments=(TextView)itemView.findViewById(R.id.comments);

        }
    }

}