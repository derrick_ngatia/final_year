package com.example.derrickngatia.revise;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.morphingbutton.MorphingButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class semester extends android.app.Fragment {

MorphingButton btn1,btn2;
String course,year;
TextView t1,t2;
    public semester() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_semester, container, false);
        course=getArguments().getString("course");
        year=getArguments().getString("year");
        t1=(TextView)view.findViewById(R.id.Name);
        t2=(TextView)view.findViewById(R.id.Name1);
        t1.setText(course);
        t2.setText(year);
        btn1=(MorphingButton) view.findViewById(R.id.btn1);
        btn2=(MorphingButton) view.findViewById(R.id.btn2);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(view.getContext(),Your_units.class);
                intent.putExtra("course",course);
                intent.putExtra("year", year);
                intent.putExtra("sem","1");
                startActivity(intent);
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(view.getContext(),Your_units.class);
                intent.putExtra("course",course);
                intent.putExtra("year", year);
                intent.putExtra("sem","2");
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        try {
            finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        super.onDestroy();
    }
}
