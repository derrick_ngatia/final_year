package com.example.derrickngatia.revise;

/**
 * Created by DERRICK NGATIA on 6/17/2018.
 */

public class document {
    private String name;
    private String url;
    private  String time;
    private String uploader;
    private String year_of_examination;


    public document() {
    }

    public document(String name, String url, String time, String uploader, String year_of_examination) {
        this.name = name;
        this.url = url;
        this.time = time;
        this.uploader = uploader;
        this.year_of_examination = year_of_examination;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public String getYear_of_examination() {
        return year_of_examination;
    }

    public void setYear_of_examination(String year_of_examination) {
        this.year_of_examination = year_of_examination;
    }
}
