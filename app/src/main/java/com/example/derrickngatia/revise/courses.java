package com.example.derrickngatia.revise;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class courses extends Fragment {
Button add,unit;
ListView Lview;
dbhelper helper;
    public courses() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_courses, container, false);
        helper=new dbhelper(getActivity());
        ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(getActivity(),R.layout.xx,R.id.text4,helper.getCourse());
        Lview=(ListView)view.findViewById(R.id.listofcourses);
        Lview.setAdapter(arrayAdapter);
        Lview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                do {
                    year1 y=new year1();
                    Bundle b=new Bundle();
                    b.putString("course",Lview.getItemAtPosition(i).toString());
                    y.setArguments(b);
                    FragmentManager manager=getFragmentManager();
                    manager.beginTransaction().replace(R.id.container89,y).addToBackStack("").commit();                }while(i==Lview.getCount());
            }
        });
          final add is=new add();
            add=(Button)view.findViewById(R.id.add);
            unit=(Button)view.findViewById(R.id.unit);
            unit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addUnits y=new addUnits();
                    FragmentManager manager=getFragmentManager();
                    manager.beginTransaction().replace(R.id.container89,y).addToBackStack("").commit();
                }
            });
             add.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        FragmentManager manager=getFragmentManager();
        manager.beginTransaction().replace(R.id.container89,is).addToBackStack("").commit();
    }
});
    return view;
    }



}
