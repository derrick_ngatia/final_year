package com.example.derrickngatia.revise;

/**
 * Created by DERRICK NGATIA on 6/23/2018.
 */

public class ResponseFeedBack {
    private String sender;
    private String Message;
    private  String Time;
    private String category;
    private int read=0;

    public ResponseFeedBack(String sender, String message, String time, String category, int read) {
        this.sender = sender;
        Message = message;
        Time = time;
        this.category = category;
        this.read = read;
    }

    public ResponseFeedBack() {
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }
}
