package com.example.derrickngatia.revise;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {
    EditText email,password;
    TextView register;
    FirebaseAuth firebaseAuth;
    dbhelper myhelper;
    ProgressDialog progressDialog;
    TextView forgot_password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Qeazy");
        getSupportActionBar().setSubtitle("Egerton Electronic Revision App");
        email=(EditText)findViewById(R.id.email);
        forgot_password=(TextView)findViewById(R.id.forgot_password);
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                com.example.derrickngatia.revise.forgot_password forgot_password=new forgot_password();
                forgot_password.show(getFragmentManager(),"");
            }
        });
        myhelper=new dbhelper(this);
        Cursor rs=myhelper.viewAll();
        firebaseAuth=FirebaseAuth.getInstance();
        if(rs.getCount()<1){
            Boolean insert=myhelper.insertData("false");

        }
        else{
            while (rs.moveToNext()){
                if (rs.getString(0).toString().compareTo("true") != 0){
                    continue;
                }
                else{
                    if (FirebaseAuth.getInstance().getCurrentUser().getEmail()=="derykowanynx@gmail.com"){
                        Intent intent=new Intent(MainActivity.this,AdminPanel.class);
                        startActivity(intent);
                    }else {
                        Intent intent=new Intent(MainActivity.this,getcourses.class);
                        startActivity(intent);
                    }

                }
            }
        }
        rs.close();
        password=(EditText)findViewById(R.id.password);
        register=(TextView)findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Registration.class);
                startActivity(intent);
            }
        });
    }

    public void sign_user(final View view) {
        progressDialog=new ProgressDialog(view.getContext());
        progressDialog.setTitle("please wait...");
        progressDialog.setMessage("Authenticating.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        if(haveNetworkConnection()==true) {
            progressDialog.setCancelable(false);
            if (email.getText().toString().isEmpty() ) {
                email.setError("Email is required");
            }else if (password.getText().toString().isEmpty()) {
                password.setError("Password is required!");

            }else
             {
                 showProgressDialog(view,true,20000);
                 firebaseAuth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            showProgressDialog(view,false,0);
                            CheckBox checkBox = (CheckBox) findViewById(R.id.remember);
                            if (checkBox.isChecked()) {
                                myhelper.updateUsername("true");
                            } else {
                                myhelper.updateUsername("false");
                            }
                            if (FirebaseAuth.getInstance().getCurrentUser().getEmail().equals("derykowaynx@gmail.com")){
                                Intent intent = new Intent(MainActivity.this, AdminPanel.class);
                                startActivity(intent);
                            }else {
                                Intent intent = new Intent(MainActivity.this, getcourses.class);
                                startActivity(intent);
                            }

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                     @Override
                     public void onFailure(@NonNull Exception e) {
                         showProgressDialog(view,false,0);
                         Snackbar.make(view,""+e.getMessage(),Snackbar.LENGTH_SHORT).show();
                     }
                 });
            }
        }
        else {
            progressDialog.dismiss();
            Snackbar.make(view,"No internet connection",Snackbar.LENGTH_SHORT).show();
        }
    }
   private void showProgressDialog(final View view, boolean show, long time) {
        try {
            if (progressDialog != null) {
                if (show) {
                    progressDialog.setMessage("please wait...");
                    progressDialog.show();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            if(progressDialog!=null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                                Snackbar.make(view,"No internet connection",Snackbar.LENGTH_LONG).show();
                            }
                        }
                    }, time);
                } else {
                    progressDialog.dismiss();
                }
            }
        }catch(IllegalArgumentException e){
        }catch(Exception e){
        }
    }
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
