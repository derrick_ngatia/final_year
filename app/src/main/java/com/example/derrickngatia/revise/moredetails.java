package com.example.derrickngatia.revise;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class moredetails extends AppCompatActivity implements SearchView.OnQueryTextListener {
    ImageView image;
    String url;
    RecyclerView recyclerView;
    FloatingActionButton button;
    EditText tag,questions;
    ProgressDialog dialog;
    ChatAdapter chatAdapter;
    ArrayList<ChatQuestion> arrayLists;
    Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moredetails);
        final TextView emptyView=(TextView)findViewById(R.id.empty_view);
        Toolbar toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView=(RecyclerView) findViewById(R.id.questions);
        dialog=new ProgressDialog(getApplicationContext());
        dialog.setMessage("retrieving discussions ...");
        dialog.setMessage("Discussion platform");
        bundle=getIntent().getExtras();
        url=bundle.getString("url");
        button=(FloatingActionButton)findViewById(R.id.fab);

        final String user= bundle.getString("name");
        arrayLists=new ArrayList<>();
        final int iend = user.indexOf(".");
        String subString = null;
        if (iend != -1)
        {
            subString= user.substring(0 , iend); //this will give abc
        }
        FirebaseDatabase.getInstance().getReference().child("chats").child("qestions").child(subString)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        arrayLists.clear();
                        for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                            ChatQuestion question=snapshot.getValue(ChatQuestion.class);
                            arrayLists.add(question);
                        }
                        chatAdapter=new ChatAdapter(arrayLists,getApplicationContext());
                        LinearLayoutManager manager=new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(manager);
                        recyclerView.setAdapter(chatAdapter);
                        if (arrayLists.isEmpty()) {
                            recyclerView.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }
                        else {
                            recyclerView.setVisibility(View.VISIBLE);
                            emptyView.setVisibility(View.GONE);
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConstraintLayout linear = (ConstraintLayout) findViewById(R.id.container9);
                final Snackbar snackbar = Snackbar.make(linear, "", Snackbar.LENGTH_INDEFINITE);
                Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View myview = layoutInflater.inflate(R.layout.q, null);
                tag=(EditText)myview.findViewById(R.id.tag);
                questions=(EditText)myview.findViewById(R.id.message);
                Button submit = (Button) myview.findViewById(R.id.post);
                Button cancel = (Button) myview.findViewById(R.id.cancel);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                    }
                });
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String subString = null;
                        if (iend != -1)
                        {
                            subString= user.substring(0 , iend); //this will give abc
                        }
                        if (tag.getText().toString().isEmpty()){
                            tag.setError("referred question is required");
                        }else if (questions.getText().toString().isEmpty()){
                            questions.setError("please add a question!");
                        }
                        else {
                            ChatQuestion question=new ChatQuestion(questions.getText().toString(), FirebaseAuth.getInstance().getCurrentUser().getEmail(),tag.getText().toString(),"");
                            FirebaseDatabase.getInstance().getReference().child("chats").child("qestions").child(subString).push().setValue(question).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(getApplicationContext(),"Question has been posted",Toast.LENGTH_SHORT).show();
                                    tag.setText("");
                                    questions.setText("");
                                    snackbar.dismiss();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getApplicationContext(),""+e.getLocalizedMessage(),Toast.LENGTH_SHORT).show();

                                }
                            });
                        }
                    }
                });
                layout.addView(myview, 0);
                snackbar.show();
            }
        });
        image=(ImageView) findViewById(R.id.image);
        Uri  uri=Uri.parse(url);
        Glide.with(getApplicationContext()).load(uri).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                image.setImageBitmap(bitmap);
            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(moredetails.this,FullScreen.class);
                intent.putExtra("image_url",url);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.questions,menu);
        MenuItem search=menu.findItem(R.id.action_search);
        android.support.v7.widget.SearchView searchView= (android.support.v7.widget.SearchView) MenuItemCompat.getActionView(search);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("search here");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        chatAdapter.getFilter().filter(newText);
        return false;
    }

}
