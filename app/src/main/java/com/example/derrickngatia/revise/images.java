package com.example.derrickngatia.revise;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class images extends Fragment{
    // Creating DatabaseReference.
    DatabaseReference databaseReference;

    // Creating RecyclerView.
    RecyclerView recyclerView;

    // Creating RecyclerView.Adapter.
    static RecyclerViewAdapter adapter;
    // Creating Progress dialog
    LinearLayoutManager mLayoutManager;
    ProgressDialog progressDialog;
    LinearLayoutManager layoutManager;

    // Creating List of upload class.
    List<upload> retrieved = new ArrayList<>();

    upload info=new upload();
    public images() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Random random=new Random();
        final View view=inflater.inflate(R.layout.fragment_images,container,false);
        final TextView emptyView=(TextView)view.findViewById(R.id.empty_view);
        final String path=getArguments().getString("path");
        final String course=getArguments().getString("course");
        String year=getArguments().getString("year");
        String sem=getArguments().getString("sem");
        /*download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    Bitmap bitmap=imageView.getDrawingCache();
                    File sdCardDirectory=new File(Environment.getExternalStorageDirectory()+File.separator+"Revise"+""+path);
                    sdCardDirectory.mkdirs();
                    String imageName="revision_"+String.valueOf(random.nextInt(1000)+System.currentTimeMillis()+".jpg");
                    File file=new File(sdCardDirectory,imageName);
                    FileOutputStream outputStream;
                    outputStream=new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
                    outputStream.flush();
                    outputStream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),"No SD Card found",Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        */
        recyclerView=(RecyclerView)view.findViewById(R.id.recylerView);
        progressDialog=new ProgressDialog(view.getContext());
        progressDialog.setTitle("please wait.....");
        progressDialog.setMessage("getting images");
        databaseReference = FirebaseDatabase.getInstance().getReference().child("allcourses").child(course).child(year).child(sem).child(path).child("images");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                retrieved.clear();
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    upload i=postSnapshot.getValue(upload.class);
                    retrieved.add(i);

                }
                ToggleButton toggle = (ToggleButton) view.findViewById(R.id.listStyle);
                toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {

                            mLayoutManager = new GridLayoutManager(getContext(),2);
                            recyclerView.setLayoutManager(mLayoutManager);
                        } else {
                            // The toggle is disabled
                            layoutManager=new LinearLayoutManager(view.getContext());
                            recyclerView.setLayoutManager(layoutManager);

                        }
                    }
                });
                mLayoutManager = new LinearLayoutManager(getContext());
                adapter=new RecyclerViewAdapter(getContext(),retrieved,course);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setAdapter(adapter);
                recyclerView.setHasFixedSize(false);
                if (retrieved.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                }
                else {
                    recyclerView.setVisibility(View.VISIBLE);
                    emptyView.setVisibility(View.GONE);
                }
                progressDialog.dismiss();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                // Hiding the progress dialog.
                progressDialog.dismiss();

            }

        });
        return view;
    }
    @Override
    public void onDestroyView() {

        retrieved.clear();
        try {
            finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        super.onDestroyView();
    }


}
