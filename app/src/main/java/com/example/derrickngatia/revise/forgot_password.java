package com.example.derrickngatia.revise;


import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;


/**
 * A simple {@link Fragment} subclass.
 */
public class forgot_password extends DialogFragment {

EditText email;
Button request;
ProgressBar progressBar;
    public forgot_password() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_forgot_password, container, false);
        email=(EditText)view.findViewById(R.id.email);
        progressBar=(ProgressBar)view.findViewById(R.id.progress);
        request=(Button) view.findViewById(R.id.request);
        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
               if (email.getText().toString().isEmpty()){
                   email.setError("Email required");
               }
               else{
                   progressBar.setVisibility(View.VISIBLE);
                   FirebaseAuth.getInstance().sendPasswordResetEmail(email.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                       @Override
                       public void onSuccess(Void aVoid) {
                           Toast.makeText(view.getContext(),"Reset url sent. Check your email",Toast.LENGTH_LONG).show();
                           progressBar.setVisibility(View.INVISIBLE);
                           dismiss();
                       }
                   }).addOnFailureListener(new OnFailureListener() {
                       @Override
                       public void onFailure(@NonNull Exception e) {
                           Toast.makeText(view.getContext(),""+e.getMessage(),Toast.LENGTH_LONG).show();
                           progressBar.setVisibility(View.INVISIBLE);
                           dismiss();
                       }
                   });
               }
            }
        });

        return view;
    }

}
