package com.example.derrickngatia.revise;

import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by DERRICK NGATIA on 10/8/2017.
 */

public class message extends DialogFragment {
   FirebaseAuth firebaseAuth;
   @Nullable
   @Override
   public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
      final View view=inflater.inflate(R.layout.redirect,container,false);
      firebaseAuth=FirebaseAuth.getInstance();
      Button redirect=(Button)view.findViewById(R.id.redirect);
      Button exit=(Button)view.findViewById(R.id.exit);
      exit.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            dismiss();
         }
      });
      redirect.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("message/rfc822");
            try {
               startActivity(Intent.createChooser(share, "pick an email client"));
            }catch (ActivityNotFoundException e){
               Toast.makeText(view.getContext(),"There no email clients install and try again",Toast.LENGTH_LONG).show();
            }
         }
      });

      return view;
   }
}
