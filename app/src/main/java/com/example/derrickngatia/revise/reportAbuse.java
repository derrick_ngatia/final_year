package com.example.derrickngatia.revise;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;


/**
 * A simple {@link Fragment} subclass.
 */
public class reportAbuse extends android.app.DialogFragment {

  Button submit;
  CheckBox offensive,non_educative;
  EditText description;
  ProgressDialog dialog;
    public reportAbuse() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_report_abuse, container, false);
        submit=(Button)view.findViewById(R.id.submit);
        dialog=new ProgressDialog(view.getContext());
         dialog.setMessage("Sending FeedBack...");
        offensive=(CheckBox)view.findViewById(R.id.offensive);
        non_educative=(CheckBox)view.findViewById(R.id.non_educative);
        description=(EditText)view.findViewById(R.id.description);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                String describe = "offensive";
                if(!offensive.isChecked() && !non_educative.isChecked() && description.getText().toString().isEmpty()){
                    description.setError("Please provide a feedback");
                }
                else {
                    dialog.show();
                    if(offensive.isChecked()){
                        describe="Offensive";
                    }else if(non_educative.isChecked()){
                        describe="None Educative Material";
                    }else if(offensive.isChecked() && !description.getText().toString().isEmpty()){
                        describe="Offensive\n\t"+description.getText().toString();
                    }
                    else if(non_educative.isChecked() && !description.getText().toString().isEmpty()){
                        describe="None Educative\n\t"+description.getText().toString();
                    }
                    report report=new report(getArguments().getString("url"),getArguments().getString("name"),getArguments().getString("time"),describe);
                   if (getArguments().getString("type").equals("image")) {
                       FirebaseDatabase.getInstance().getReference().child("reports").child("images").push().setValue(report).addOnCompleteListener(new OnCompleteListener<Void>() {
                           @Override
                           public void onComplete(@NonNull Task<Void> task) {
                               dialog.dismiss();
                               dismiss();
                               Toast.makeText(view.getContext(), "Feedback sent successfully", Toast.LENGTH_SHORT).show();

                           }
                       }).addOnFailureListener(new OnFailureListener() {
                           @Override
                           public void onFailure(@NonNull Exception e) {
                               dialog.dismiss();
                               Toast.makeText(view.getContext(), "Error sending Feedback " + e.getMessage(), Toast.LENGTH_SHORT).show();
                           }
                       });
                   }
                   else if(getArguments().getString("type").equals("pdf")){
                       FirebaseDatabase.getInstance().getReference().child("reports").child("pdf").push().setValue(report).addOnCompleteListener(new OnCompleteListener<Void>() {
                           @Override
                           public void onComplete(@NonNull Task<Void> task) {
                               dialog.dismiss();
                               dismiss();
                               Toast.makeText(view.getContext(), "Feedback sent successfully", Toast.LENGTH_SHORT).show();

                           }
                       }).addOnFailureListener(new OnFailureListener() {
                           @Override
                           public void onFailure(@NonNull Exception e) {
                               dialog.dismiss();
                               Toast.makeText(view.getContext(), "Error sending Feedback " + e.getMessage(), Toast.LENGTH_SHORT).show();
                           }
                       });
                   }
                }
            }
        });
        return view;
    }

}
