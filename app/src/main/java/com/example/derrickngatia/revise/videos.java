package com.example.derrickngatia.revise;

/**
 * Created by DERRICK NGATIA on 6/23/2018.
 */

public class videos {
    private String publishedAt;
    private String tittle;
    private String description;
    private String thumbNails;
    private String videoId;

    public videos(String publishedAt, String tittle, String description, String thumbNails, String videoId) {
        this.publishedAt = publishedAt;
        this.tittle = tittle;
        this.description = description;
        this.thumbNails = thumbNails;
        this.videoId = videoId;
    }

    public videos() {
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbNails() {
        return thumbNails;
    }

    public void setThumbNails(String thumbNails) {
        this.thumbNails = thumbNails;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
