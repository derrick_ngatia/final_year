package com.example.derrickngatia.revise;

import android.view.View;

/**
 * Created by DERRICK NGATIA on 3/17/2018.
 */

interface ClickListener {
    void onItemClick(int position, View v);
}
