package com.example.derrickngatia.revise;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dd.morphingbutton.MorphingButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class m_year extends android.app.Fragment {

    int mMorphCounter1 = 1;
    int mMorphCounter2 = 1;
    semester sem;
    MorphingButton btn1,btn2,btn3,btn4,btn5,btn6;
    TextView label;
    public m_year() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_m_year, container, false);
        sem=new semester();
        label=(TextView)view.findViewById(R.id.Name);
        String courses=getArguments().getString("course");
        label.setText(courses);
        btn1=(MorphingButton) view.findViewById(R.id.btn1);
        btn2=(MorphingButton) view.findViewById(R.id.btn2);
        btn3=(MorphingButton) view.findViewById(R.id.btn3);
        btn4=(MorphingButton) view.findViewById(R.id.btn4);
        btn5=(MorphingButton) view.findViewById(R.id.btn5);
        btn6=(MorphingButton) view.findViewById(R.id.btn6);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMorphButton1Clicked(btn1,"1");

            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMorphButton1Clicked(btn2,"2");
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMorphButton1Clicked(btn3,"3");
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMorphButton1Clicked(btn4,"4");
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMorphButton1Clicked(btn4,"5");
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMorphButton1Clicked(btn4,"6");
            }
        });
        return  view;
    }
    private void onMorphButton1Clicked(MorphingButton btn1,String year) {
        if (mMorphCounter1 == 0) {
            mMorphCounter1++;
        } else if (mMorphCounter1 == 1) {
            mMorphCounter1 = 0;
            morphToSuccess(btn1,year);
        }
    }
    private void morphToSuccess(final MorphingButton btnMorph, final String year) {
        MorphingButton.Params circle = MorphingButton.Params.create()
                .duration(1000)
                .cornerRadius(50)
                .width(190)
                .height(60)
                .text("Continue")
                .color(R.color.red)
                .colorPressed(R.color.orange)
                .icon(R.drawable.check);
        btnMorph.morph(circle);
        btnMorph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String courses=getArguments().getString("course");
                Bundle bundle=new Bundle();
                bundle.putString("course",courses);
                bundle.putString("year",year);
                sem.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.container,sem).addToBackStack("").commit();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
