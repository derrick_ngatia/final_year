package com.example.derrickngatia.revise;

/**
 * Created by DERRICK NGATIA on 10/15/2017.
 */

public class image {
    private String imageName;
    private String imageURL;
    private Long imageTime;
    private String sender;
    private String year_of_examination;


    public image() {
    }

    public image(String imageName, String imageURL, Long imageTime, String sender, String year_of_examination) {
        this.imageName = imageName;
        this.imageURL = imageURL;
        this.imageTime = imageTime;
        this.sender = sender;
        this.year_of_examination = year_of_examination;
    }


    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Long getImageTime() {
        return imageTime;
    }

    public void setImageTime(Long imageTime) {
        this.imageTime = imageTime;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getYear_of_examination() {
        return year_of_examination;
    }

    public void setYear_of_examination(String year_of_examination) {
        this.year_of_examination = year_of_examination;
    }
}
