package com.example.derrickngatia.revise;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class units extends Fragment{
ListView views;
dbhelper helper;
DatabaseReference reference= FirebaseDatabase.getInstance().getReference();

    public units() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final String course=getArguments().getString("course");
        final String year=getArguments().getString("year");
        final String sem=getArguments().getString("sem");
        View view=inflater.inflate(R.layout.fragment_units2, container, false);
        views=(ListView)view.findViewById(R.id.all_units);
        helper=new dbhelper(getActivity());
        ArrayList<String> m=helper.getUnits(course,Integer.parseInt(year),Integer.parseInt(sem));
        Toast.makeText(view.getContext(),""+m,Toast.LENGTH_SHORT).show();
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(view.getContext(),R.layout.xx,R.id.text4,m);
        views.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, final View view, int i, long l) {
                final String unit=views.getItemAtPosition(i).toString();
                final AlertDialog.Builder dialog=new AlertDialog.Builder(view.getContext());
                dialog.setPositiveButton("set Course", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        reference.child("allcourses").child(course).child(year).child(sem).child(unit).child("status").setValue("set");
                        Toast.makeText(view.getContext(),""+unit,Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                         dialogInterface.dismiss();
                    }
                });
               dialog.show();
            }
        });
        views.setAdapter(adapter);
        return view;
    }

}
