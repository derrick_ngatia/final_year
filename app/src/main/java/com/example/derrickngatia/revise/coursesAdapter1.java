package com.example.derrickngatia.revise;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.github.ivbaranov.mli.MaterialLetterIcon;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by DERRICK NGATIA on 3/17/2018.
 */

public class coursesAdapter1 extends RecyclerView.Adapter<coursesAdapter1.ViewHolder> implements Filterable{
    private static final Random RANDOM = new Random();
    public static ClickListener clickListener;
    ArrayList<String> course;
    ArrayList<String> filtered;
   Context context;
   FragmentManager fragmentManager;
   ArrayList<String> data;
   int[] colors=
           {
                   R.color.AliceBlue,R.color.AntiqueWhite,R.color.Aqua,R.color.Bisque,R.color.Fuchsia,R.color.ForestGreen,R.color.BlanchedAlmond

           };

    public coursesAdapter1(ArrayList<String> course,ArrayList<String> data, Context context,FragmentManager fragmentManager) {
        this.course = course;
        this.filtered =course;
        this.context = context;
        this.data=data;
        this.fragmentManager=fragmentManager;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlist_item, parent, false);

        ViewHolder holder=new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final String name=course.get(position);
        holder.imageNameTextView.setText("  "+name);
        holder.imageView.setInitials(true);
        holder.imageView.setLetterSize(18);
        holder.imageView.setShapeColor(colors[RANDOM.nextInt(colors.length)]);
        holder.imageView.setShapeType(MaterialLetterIcon.Shape.CIRCLE);
        holder.imageView.setLetter(name);
        holder.imageNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name=course.get(position);
                Intent intent=new Intent(view.getContext(),Materials.class);
                intent.putExtra("storagePath",name);
                intent.putExtra("course",data.get(0));
                intent.putExtra("year", data.get(1));
                intent.putExtra("sem",data.get(2));
                view.getContext().startActivity(intent);
            }
        });
        holder.imageNameTextView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                 Favorite favorite=new Favorite();
                Bundle bundle=new Bundle();
                bundle.putString("storagePath",name);
                bundle.putString("course",data.get(0));
                bundle.putString("year", data.get(1));
                bundle.putString("sem",data.get(2));
                favorite.setArguments(bundle);
                favorite.show(fragmentManager,"");
                return true;
            }
        });


    }
    public void setOnItemClickListener(ClickListener clickListener) {
        coursesAdapter1.clickListener = clickListener;
    }


    @Override
public int getItemCount() {
 return filtered.size();

        }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filtered=course;
                } else {
                    ArrayList<String> filteredList = new ArrayList<>();
                    for (String list: course) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (list.contains(charSequence)) {
                            filteredList.add(list);
                        }
                    }

                    filtered = filteredList;
                }


                FilterResults filterResults = new FilterResults();
                filterResults.values = filtered;
                return filterResults;
            }
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filtered= (ArrayList<String>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
    public  TextView imageNameTextView;
    public MaterialLetterIcon imageView;
    public Button download;
    public ViewHolder(View itemView) {
        super(itemView);
        download=(Button)itemView.findViewById(R.id.download);
        imageView = (MaterialLetterIcon) itemView.findViewById(R.id.listIcon);
        imageNameTextView = (TextView) itemView.findViewById(R.id.courses);
    }

}
}