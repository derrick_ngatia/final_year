package com.example.derrickngatia.revise;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReportedDocuments extends Fragment {

RecyclerView recyclerView;
    public ReportedDocuments() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_reported_documents, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.recylerView);
        return view;
    }

}
