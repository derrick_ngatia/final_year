package com.example.derrickngatia.revise;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.AnyRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Registration extends AppCompatActivity {
    EditText username,pass1,pass2,name;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        username=(EditText)findViewById(R.id.username);
        pass1=(EditText)findViewById(R.id.pass1);
        pass2=(EditText)findViewById(R.id.pass2);
        name=(EditText)findViewById(R.id.Name);
        firebaseAuth=FirebaseAuth.getInstance();
    }

    public void Register_user(final View view) {
        final ProgressDialog progressDialog=new ProgressDialog(view.getContext());
        progressDialog.setTitle("please wait...");
        progressDialog.setMessage("processing .....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        if(username.getText().toString().isEmpty() ){
            username.setError("Email is required");
        }
        else if ( pass1.getText().toString().isEmpty() ){
            pass1.setError("Enter password!");
        }
        else if ( name.getText().toString().isEmpty() ){
            name.setError("Enter Name!");
        }
        else if (pass2.getText().toString().isEmpty()){
            pass2.setError("Confirm your password!!");
        }
        else if(!pass1.getText().toString().equals(pass2.getText().toString())){
            pass1.setError("Password don't match");
            pass2.setError("Password don't match");
        }
        else{
            progressDialog.show();
            firebaseAuth.createUserWithEmailAndPassword(username.getText().toString(),pass2.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                   @Override
                   public void onComplete(@NonNull Task<AuthResult> task) {
                       if(task.isSuccessful()) {
                           final String logFileName = new SimpleDateFormat("yyyyMMddHHmm'.pdf'").format(new Date());

                           FirebaseStorage.getInstance().getReference().child("profiles/"+logFileName).putFile(getUriToDrawable(getApplicationContext(),R.drawable.usericon)).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                               @Override
                               public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                   profile profile=new profile(name.getText().toString(),taskSnapshot.getDownloadUrl().toString());
                                   String user=username.getText().toString();
                                   int iend = user.indexOf("@");

                                   String subString = null;
                                   if (iend != -1)
                                   {
                                       subString= user.substring(0 , iend); //this will give abc
                                   }

                                   FirebaseDatabase.getInstance().getReference().child("profiles").child(subString.toLowerCase()).setValue(profile).addOnSuccessListener(new OnSuccessListener<Void>() {
                                       @Override
                                       public void onSuccess(Void aVoid) {
                                           progressDialog.dismiss();
                                           Toast.makeText(getApplicationContext(), "Account created successfully", Toast.LENGTH_SHORT).show();
                                           if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                               finishAffinity();
                                           }
                                           Intent intent=new Intent(Registration.this,MainActivity.class);
                                           startActivity(intent);
                                       }
                                   }).addOnFailureListener(new OnFailureListener() {
                                       @Override
                                       public void onFailure(@NonNull Exception e) {

                                       }
                                   });

                               }
                           }).addOnFailureListener(new OnFailureListener() {
                               @Override
                               public void onFailure(@NonNull Exception e) {
                                   Toast.makeText(getApplicationContext(),""+e,Toast.LENGTH_SHORT).show();
                               }
                           });

                           }


                   }
               }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(),""+e.getMessage(),Toast.LENGTH_LONG).show();
                }
            });
        }
    }
    public static final Uri getUriToDrawable(@NonNull Context context,
                                             @AnyRes int drawableId) {
        Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + context.getResources().getResourcePackageName(drawableId)
                + '/' + context.getResources().getResourceTypeName(drawableId)
                + '/' + context.getResources().getResourceEntryName(drawableId) );
        return imageUri;
    }

    private void verifyEmail(View view) {
        final ProgressDialog progressDialog=new ProgressDialog(view.getContext());
        progressDialog.setTitle("please wait...");
        progressDialog.setMessage("Sending Email verification link .....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
        Intent intent=new Intent(Registration.this,MainActivity.class);
        startActivity(intent);
    }
}
