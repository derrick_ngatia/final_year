package com.example.derrickngatia.revise;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.thekhaeng.recyclerviewmargin.LayoutMarginDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import static com.example.derrickngatia.revise.R.id.about;
import static com.example.derrickngatia.revise.R.id.exitAccount;
import static com.example.derrickngatia.revise.R.id.profile;
import static com.example.derrickngatia.revise.R.id.share;


/**
 * A simple {@link Fragment} subclass.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class getThem extends Fragment implements android.support.v7.widget.SearchView.OnQueryTextListener {
    ProgressDialog progressDialog;
    RecyclerView mylistv;
    Context context;
    dbhelper helper;
    View view;
    TextView results;
    static coursesAdapter adapter;
    ArrayList<gcourses> mylist = new ArrayList<gcourses>();
   final DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
    public getThem() {
        // Required empty public constructor
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.fragment_get_them, container, false);
        final TextView emptyView=(TextView) view.findViewById(R.id.empty_view);
        context=view.getContext();
        helper=new dbhelper(view.getContext());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("loading");
        progressDialog.setMessage("please wait ......");
        results=(TextView)view.findViewById(R.id.results);
        progressDialog.setCancelable(false);
        if (haveNetworkConnection() == true) {
            showProgressDialog(true,10000);
           myRef.child("allcourses").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                     mylist.clear();
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        showProgressDialog(false,0);
                        String myvalue = postSnapshot.getKey();
                        gcourses getcourses=new gcourses(myvalue);
                            mylist.add(getcourses);

                    }
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                    mylistv.setLayoutManager(mLayoutManager);
                    mylistv.setHasFixedSize(true);
                    adapter = new coursesAdapter( mylist,getActivity());
                    mylistv.setAdapter(adapter);
                    if (mylist.isEmpty()) {
                        mylistv.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                    }
                    else {
                        mylistv.setVisibility(View.VISIBLE);
                        emptyView.setVisibility(View.GONE);
                    }

                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    showProgressDialog(false,0);
                    Toast.makeText(getContext(), "" + databaseError, Toast.LENGTH_SHORT).show();
                }


            });


        }
        else{
            progressDialog.dismiss();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            retryConnection semester = new retryConnection();
            fragmentTransaction.replace(R.id.container, semester);
            fragmentTransaction.addToBackStack("").commit();
        }
        mylistv = (RecyclerView) view.findViewById(R.id.listView);
        myRef.onDisconnect();
        return view;
    }
    private void showProgressDialog(boolean show, long time) {
        try {
            if (progressDialog != null) {
                if (show) {
                    progressDialog.setMessage("please wait...");
                    progressDialog.show();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            if(progressDialog!=null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                                FragmentManager fragmentManager = getFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                retryConnection semester = new retryConnection();
                                fragmentTransaction.replace(R.id.container, semester);
                                fragmentTransaction.addToBackStack("").commit();
                            }
                        }
                    }, time);
                } else {
                    progressDialog.dismiss();
                }
            }
        }catch(IllegalArgumentException e){
        }catch(Exception e){
        }
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.signout,menu);
        MenuItem search=menu.findItem(R.id.action_search);
        android.support.v7.widget.SearchView searchView= (android.support.v7.widget.SearchView) MenuItemCompat.getActionView(search);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("search here");
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==exitAccount){

            LinearLayout linear = (LinearLayout) getActivity().findViewById(R.id.container);
            final Snackbar snackbar = Snackbar.make(linear, "", Snackbar.LENGTH_INDEFINITE);
            Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View myview = layoutInflater.inflate(R.layout.resetpass, null);
            TextView email=(TextView)myview.findViewById(R.id.email);
            email.setText("Sign Out Account: "+FirebaseAuth.getInstance().getCurrentUser().getEmail().toString()+"?");
            Button exit = (Button) myview.findViewById(R.id.cancel);
            exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });

            Button submit = (Button) myview.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FirebaseAuth.getInstance().signOut();
                    helper.updateUsername("false");
                    try {
                        finalize();
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                    Intent intent=new Intent(getActivity(),MainActivity.class);
                    startActivity(intent);
                }
            });
            layout.addView(myview, 0);
            snackbar.show();

        }
        else if(id==share) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("message/rfc822");
            share.putExtra(Intent.EXTRA_SUBJECT, "qeazy app revise at the comfort of your room");
            share.putExtra(Intent.EXTRA_TEXT, "download qeazy app on google store today at https://googlestore.com/qezy");
            try {
                startActivity(Intent.createChooser(share, "pick an email client"));
            }catch (ActivityNotFoundException e){
                Toast.makeText(getActivity(),"There no email clients install and try again",Toast.LENGTH_LONG).show();
            }
        }
        else if(id==about){
            Fback fback=new Fback();
            fback.show(getFragmentManager(),"");
        }
        else if (id==profile){
            Myprofile myprofile=new Myprofile();
            myprofile.show(getFragmentManager(),"");
        }
        // else if(id==uploading){
        //Intent intent=new Intent(getcourses.this,addmaterials.class);
        //startActivity(intent);
        // }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }
}
