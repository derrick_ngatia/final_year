package com.example.derrickngatia.revise;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLDisplay;

public class Replies extends AppCompatActivity {
TextView sender, time,tag,message;
FloatingActionButton button;
EditText editText;
ProgressDialog dialog;
RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replies);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final Bundle bundle= getIntent().getExtras();
        time=(TextView)findViewById(R.id.time);
        dialog=new ProgressDialog(getApplicationContext());
        dialog.setMessage("Posting reply ....");
        sender=(TextView)findViewById(R.id.sender);
        tag=(TextView)findViewById(R.id.tag);
        message=(TextView)findViewById(R.id.message);
        time.setText(bundle.getString("time"));
        sender.setText(bundle.getString("sender"));
        tag.setText("#Question "+bundle.getString("tag"));
        message.setText(bundle.getString("message"));
        editText=(EditText)findViewById(R.id.answer);
        button=(FloatingActionButton)findViewById(R.id.fab);
         button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().toString().isEmpty()){
                    editText.setError("Reply required..");
                }else{
                    ChatReply reply=new ChatReply(FirebaseAuth.getInstance().getCurrentUser().getEmail(),editText.getText().toString());
                    FirebaseDatabase.getInstance().getReference().child("replies").child(bundle.getString("id")).push().setValue(reply).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(),"Posted successfully",Toast.LENGTH_SHORT).show();
                            editText.setText("");
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(),"Post failed "+e.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });



    }

    @Override
    protected void onStart() {
        super.onStart();
        final ArrayList<ChatReply> replies=new ArrayList<>();
        final TextView emptyView=(TextView)findViewById(R.id.empty_view);
        recyclerView=(RecyclerView)findViewById(R.id.recylerView);
        final Bundle bundle= getIntent().getExtras();
        FirebaseDatabase.getInstance().getReference().child("replies").child(bundle.getString("id")).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                replies.clear();
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    ChatReply reply=snapshot.getValue(ChatReply.class);
                    replies.add(reply);
                }
                ReplyAdapter adapter=new ReplyAdapter(replies,getApplicationContext());
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                recyclerView.setAdapter(adapter);
                if (replies.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                }
                else {
                    recyclerView.setVisibility(View.VISIBLE);
                    emptyView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
