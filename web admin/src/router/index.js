import Vue from 'vue'
import Router from 'vue-router'
import  Login from '@/components/login'
import dashboard from '@/components/dashboard'
import firebase from 'firebase'
import courses from '@/components/courses'
import edit from '@/components/edit'
import addcourse from '@/components/addcourse'
import add_unit from '@/components/add_unit'
import feedback from '@/components/feedback'






Vue.use(Router)
Vue.component('dashboard',dashboard)

let router= new Router({
  routes: [
    {
        path: '/',
        name: 'Login',
        component: Login
    },
      {
           path: '*',
          redirect: '/login'
      },

     {
          path: '/login',
          name: 'Login',
          component: Login
      }
      ,
      {
          path: '/dashboard',
          name:'dashboard',
          component: dashboard,
          meta: {
              requiresAuth: true
          }

      },
      {
          path: '/courses',
          name:'courses',
          component: courses,
          meta: {
              requiresAuth: true
          }

      },
      {
          path: '/edit/{course}',
          name:'edit',
          component: edit,
          meta: {
              requiresAuth: true
          }

      },
      {
          path: '/addcourse',
          name:'addcourse',
          component: addcourse,
          meta: {
              requiresAuth: true
          }

      },{
          path: '/add_unit',
          name:'addunit',
          component: add_unit,
          meta: {
              requiresAuth: true
          },


      },
      {
          path: '/feedback',
          name:'feedback',
          component: feedback,
          meta: {
              requiresAuth: true
          },


      }
  ]
})
router.beforeEach((to,from,next)=>{
    let currentUser=firebase.auth().currentUser;
    let requiresAuth=to.matched.some(record=>record.meta.requiresAuth);
    if (requiresAuth && !currentUser){
        next('login')
    }else if(!requiresAuth && currentUser){
        next('dashboard')
    }
    else {
        next()
    }
})
export default router