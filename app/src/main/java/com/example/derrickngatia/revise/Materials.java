package com.example.derrickngatia.revise;

import android.app.Activity;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Locale;

public class Materials extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final int IMAGE_PICK_REQUEST =1234 ;
    private static final int IMAGE_PICK_REQUEST1 =1235 ;
    private static final int IMAGE_PICK_REQUEST2 =1236 ;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    TabLayout tabLayout;
    private ViewPager mViewPager;
    Bundle bundle;
    private Uri filepath;
    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materials);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        tabLayout.setupWithViewPager(mViewPager);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            if (position==2){
                fab.setVisibility(View.INVISIBLE);

            }
            else if(position==0){
                fab.setVisibility(View.VISIBLE);
            }
            else if(position==1){
                fab.setVisibility(View.VISIBLE);

            }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu menu=new PopupMenu(view.getContext(),view);
                menu.inflate(R.menu.select);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.gallery:
                                Intent intent=new Intent();
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                intent.setType("image/*");
                                startActivityForResult(Intent.createChooser(intent,"select an image"),IMAGE_PICK_REQUEST);
                                return true;
                            case R.id.camera:
                                Intent pictureIntent = new Intent(
                                        MediaStore.ACTION_IMAGE_CAPTURE
                                );
                                if(pictureIntent.resolveActivity(getPackageManager()) != null) {
                                    startActivityForResult(pictureIntent,
                                            IMAGE_PICK_REQUEST1);
                                }
                                return true;
                            case R.id.pdf:
                                String[] mimeTypes =
                                        {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                                                "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                                                "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                                                "text/plain",
                                                "application/pdf",
                                                "application/zip"};

                                Intent document = new Intent(Intent.ACTION_GET_CONTENT);
                                document.addCategory(Intent.CATEGORY_OPENABLE);

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                    document.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
                                    if (mimeTypes.length > 0) {
                                        document.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                                    }
                                } else {
                                    String mimeTypesStr = "";
                                    for (String mimeType : mimeTypes) {
                                        mimeTypesStr += mimeType + "|";
                                    }
                                    document.setType(mimeTypesStr.substring(0,mimeTypesStr.length() - 1));
                                }
                                startActivityForResult(Intent.createChooser(document,"ChooseFile"), IMAGE_PICK_REQUEST2);

                                return true;
                            default:
                                return false;
                        }

                    }
                });

                menu.show();
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_materials, menu);
        MenuItem search=menu.findItem(R.id.action_search);
        android.support.v7.widget.SearchView searchView= (android.support.v7.widget.SearchView) MenuItemCompat.getActionView(search);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("search here");
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==IMAGE_PICK_REQUEST && resultCode==RESULT_OK && data!=null && data.getData()!=null){

            filepath=data.getData();
            try {
                Bitmap map= MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(),filepath);
                UploadToFirebase uploadToFirebase=new UploadToFirebase();
                bundle=getIntent().getExtras();
                Bundle bundles=new Bundle();
                String path=bundle.getString("storagePath");
                String course=bundle.getString("course");
                String year=bundle.getString("year");
                String sem=bundle.getString("sem");
                bundles.putString("path",path);
                bundles.putString("course",course);
                bundles.putString("year",year);
                bundles.putString("sem",sem);
                bundles.putParcelable("image",map);
                bundles.putParcelable("filepath",filepath);
                uploadToFirebase.setArguments(bundles);
                uploadToFirebase.show(getFragmentManager(),"");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        if (requestCode == IMAGE_PICK_REQUEST1 &&
                resultCode == RESULT_OK) {
            if (data != null && data.getExtras() != null) {
                filepath=data.getData();
                try {
                    Bitmap map= MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(),filepath);
                    UploadToFirebase uploadToFirebase=new UploadToFirebase();
                    bundle=getIntent().getExtras();
                    Bundle bundles=new Bundle();
                    String path=bundle.getString("storagePath");
                    String course=bundle.getString("course");
                    String year=bundle.getString("year");
                    String sem=bundle.getString("sem");
                    bundles.putString("path",path);
                    bundles.putString("course",course);
                    bundles.putString("year",year);
                    bundles.putString("sem",sem);
                    bundles.putParcelable("image",map);
                    bundles.putParcelable("filepath",filepath);
                    uploadToFirebase.setArguments(bundles);
                    uploadToFirebase.show(getFragmentManager(),"");

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        if(requestCode==IMAGE_PICK_REQUEST2 && resultCode==RESULT_OK && data!=null && data.getData()!=null){
              filepath=data.getData();
            Uploadpdf pdf=new Uploadpdf();
            bundle=getIntent().getExtras();
            Bundle bundles=new Bundle();
            String path=bundle.getString("storagePath");
            String course=bundle.getString("course");
            String year=bundle.getString("year");
            String sem=bundle.getString("sem");
            bundles.putString("path",path);
            bundles.putString("course",course);
            bundles.putString("year",year);
            bundles.putString("sem",sem);
            bundles.putString("name",getRealPathFromUri(filepath));
            bundles.putParcelable("filepath",filepath);
            pdf.setArguments(bundles);
            pdf.show(getFragmentManager(),"");

        }
    }
    private String getRealPathFromUri(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id==R.id.profile){
            Myprofile myprofile=new Myprofile();
            myprofile.show(getFragmentManager(),"");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (mViewPager.getCurrentItem()==0){
            images images=new images();
            images.adapter.getFilter().filter(newText);

        }else if (mViewPager.getCurrentItem()==1){
           pdf pdf=new pdf();
           pdf.adapter.getFilter().filter(newText);

        }
        else if (mViewPager.getCurrentItem()==2){
            video pdf=new video();
            pdf.adapter.getFilter().filter(newText);

        }
        return false;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            bundle=getIntent().getExtras();
            Bundle bundle1=new Bundle();
            String path=bundle.getString("storagePath");
            String course=bundle.getString("course");
            String year=bundle.getString("year");
            String sem=bundle.getString("sem");
            bundle1.putString("path",path);
            bundle1.putString("course",course);
            bundle1.putString("year",year);
            bundle1.putString("sem",sem);
            switch (position){
                case 0:
                    images tab1=new images();
                    tab1.setArguments(bundle1);
                    return tab1;
                case 1:
                    pdf tab2=new pdf();
                    tab2.setArguments(bundle1);
                    return  tab2;
                case 2:
                    video tab3=new video();
                    tab3.setArguments(bundle1);
                    return  tab3;
                default:
                    return null;

            }

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            Drawable mydrawable;
            SpannableStringBuilder sb;
            ImageSpan span;
            switch (position) {
                case 0:
                    mydrawable=getResources().getDrawable(R.drawable.images);
                    sb=new SpannableStringBuilder(" Images");
                    mydrawable.setBounds(0,0,mydrawable.getIntrinsicWidth(),mydrawable.getIntrinsicHeight());
                    span=new ImageSpan(mydrawable,ImageSpan.ALIGN_BASELINE);
                    sb.setSpan(span,0,1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    return sb;
                case 1:
                    mydrawable=getResources().getDrawable(R.drawable.pdfs);
                    sb=new SpannableStringBuilder(" PDF's");
                    mydrawable.setBounds(0,0,mydrawable.getIntrinsicWidth(),mydrawable.getIntrinsicHeight());
                    span=new ImageSpan(mydrawable,ImageSpan.ALIGN_BASELINE);
                    sb.setSpan(span,0,1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    return sb;
                case 2:
                    mydrawable=getResources().getDrawable(R.drawable.videos);
                    sb=new SpannableStringBuilder(" Videos");
                    mydrawable.setBounds(0,0,mydrawable.getIntrinsicWidth(),mydrawable.getIntrinsicHeight());
                    span=new ImageSpan(mydrawable,ImageSpan.ALIGN_BASELINE);
                    sb.setSpan(span,0,1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    return sb;

            }
            return null;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
