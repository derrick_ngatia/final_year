package com.example.derrickngatia.revise;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by DERRICK NGATIA on 8/17/2017.
 */
public class dbhelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "stash";
    public static final int Version = 1;
    public static final String tableName = "users";
    public dbhelper(Context context) {
        super(context, DB_NAME, null, Version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table users(Remember TEXT )");
        db.execSQL("create table courses(courseId integer unique,courseName text not null)");
        db.execSQL("create table yearandSem(course integer ,unitname text not null,year smallint not null,sem smallint not null, foreign key(course) references courses(courseId) )");
        //db.execSQL("create table loginStatus(status TEXT )");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS users");
        db.execSQL("drop table courses");
        db.execSQL("drop table yearandSem");
        onCreate(db);
    }

    public Boolean insertData(String username) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("Remember", username);
        long insert=db.insert(tableName, null, values);
        if (insert != -1) return true;
        else return false;
    }

    public Cursor viewAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor rs = db.rawQuery(" select * from users ", null);
        return rs;
    }
    public void updateUsername(String username){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE users SET Remember='"+username+"' ");

    }
    public Boolean add(int courseid,String CourseName,String unitname,int year,int sem){
        SQLiteDatabase db=getWritableDatabase();
        ContentValues values=new ContentValues();
        ContentValues values1=new ContentValues();
        values.put("courseId", courseid);
        values.put("courseName",CourseName);
        long insert=db.insert("courses",null,values);
        if(insert==-1) {
            return false;
        }
        else{
            Boolean in=insertin(courseid,unitname,year,sem);
            if(in==true){
                return true;

            }else {
                return false;
            }
        }

    }

    public Boolean insertin(int courseid, String unitname, int year, int sem) {
        SQLiteDatabase db=getWritableDatabase();
        ContentValues values=new ContentValues();
        ContentValues values1=new ContentValues();
        values.put("course", courseid);
        values.put("unitname",unitname);
        values.put("year",year);
        values.put("sem",sem);
        long insert=db.insert("yearandSem",null,values);
        if(insert==-1)
            return false;
        else
            return true;
    }
    public ArrayList<String> getCourse(){
        ArrayList<String> ln=new ArrayList<>();
        SQLiteDatabase database=this.getWritableDatabase();
        Cursor rs=database.rawQuery("select * from courses",null);
        rs.isFirst();
        while (rs.moveToNext()){
            ln.add(rs.getString(1));
        }
        return ln;
    }
    public ArrayList<String> getUnits(String course,int year,int sem)
    { ArrayList<String> mylist=new ArrayList<>();
        SQLiteDatabase database=this.getWritableDatabase();
        Cursor rs=database.rawQuery(" SELECT Y.unitname FROM courses S,yearandSem Y WHERE S.courseId=Y.course and S.courseName='"+course+"' and Y.year='"+year+"' and Y.sem='"+sem+"'",null);
        while (rs.moveToNext()){
            String unit=rs.getString(0);
            mylist.add(unit);
        }
        return mylist;
    }
    public String getCourseId(String course){
        SQLiteDatabase database=this.getWritableDatabase();
        String id = null;
        Cursor rs=database.rawQuery("select * from courses where courseName='"+course+"'",null);
        while (rs.moveToNext()){
            id=rs.getString(0);
        }

        return id;
    }
}

