package com.example.derrickngatia.revise;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class pdf_Notifications extends AppCompatActivity {
    WebView webView;
    ProgressBar progressBar;
    TextView textView, time;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf__notifications);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar=(ProgressBar)findViewById(R.id.progress);
        webView=(WebView)findViewById(R.id.pdf);
        setSupportActionBar(toolbar);
        try {
            progressBar.setVisibility(View.VISIBLE);
            String urlEncoded = URLEncoder.encode(getIntent().getExtras().getString("url"), "UTF-8");
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            webView.loadUrl("http://docs.google.com/viewer?url=" +urlEncoded);
            progressBar.setVisibility(View.INVISIBLE);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        textView=(TextView)findViewById(R.id.name) ;
        time=(TextView)findViewById(R.id.time);
        textView.setText(getIntent().getExtras().getString("name"));
        time.setText(DateFormat.format("yyyy-MM-dd hh:mm:ss a", Long.parseLong(getIntent().getExtras().getString("time"))).toString());
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getIntent().getExtras().getString("url")));
                view.getContext().startActivity(browserIntent);
            }
        });
    }

}
