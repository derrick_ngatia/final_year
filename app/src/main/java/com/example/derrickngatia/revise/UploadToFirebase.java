package com.example.derrickngatia.revise;


import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class UploadToFirebase extends DialogFragment {
    DatabaseReference fire= FirebaseDatabase.getInstance().getReference();
    private StorageReference mStorageRef;
  ImageView imageView;
  ProgressDialog dialog;
  TextView course,year,semester,unit_name;
  Button upload;
  EditText year_of_examination;

    public UploadToFirebase() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_upload_to_firebase, container, false);
        upload=(Button)view.findViewById(R.id.upload);
        year_of_examination=(EditText)view.findViewById(R.id.year_of_examination);
        imageView=(ImageView)view.findViewById(R.id.image);
        course=(TextView)view.findViewById(R.id.course);
        year=(TextView)view.findViewById(R.id.year);
        semester=(TextView)view.findViewById(R.id.semester);
        dialog=new ProgressDialog(view.getContext());
        dialog.setTitle("Uploading ......");
        dialog.setCancelable(false);
        unit_name=(TextView)view.findViewById(R.id.unit_name);
        Bitmap bitmapimage =getArguments().getParcelable("image");
        final Uri filepath=getArguments().getParcelable("filepath");
        imageView.setImageBitmap(bitmapimage);
        course.setText(getArguments().getString("course"));
        year.setText(getArguments().getString("year"));
        semester.setText(getArguments().getString("sem"));
        unit_name.setText(getArguments().getString("path"));
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if(year_of_examination.getText().toString().isEmpty()){
                    year_of_examination.setError("year of examination required");
                }else {
                    dialog.show();
                    mStorageRef = FirebaseStorage.getInstance().getReference();
                    final String logFileName = new SimpleDateFormat("yyyyMMddHHmm'.jpg'").format(new Date());

                    StorageReference riversRef = mStorageRef.child(getArguments().getString("path") + "/" + logFileName);
                    riversRef.putFile(filepath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getActivity(), "file uploaded", Toast.LENGTH_LONG).show();
                            upload upload = new upload(logFileName, taskSnapshot.getDownloadUrl().toString(), "0",year_of_examination.getText().toString());
                            fire.child("allcourses").child(getArguments().getString("course")).child(getArguments().getString("year"))
                                    .child(getArguments().getString("sem"))
                                    .child(getArguments().getString("path"))
                                    .child("images").push()
                                    .setValue(upload).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    dialog.dismiss();
                                    Snackbar.make(view, "File Uploaded", Snackbar.LENGTH_SHORT).show();
                                    dismiss();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    dialog.dismiss();
                                    Snackbar.make(view, "" + e.getMessage(), Snackbar.LENGTH_SHORT).show();
                                }
                            });

                        }
                    }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage(progress + "% uploaded ....");
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            dialog.dismiss();
                            Snackbar.make(view, "File Could not be Uploaded " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
                        }
                    });

                }
            }
        });
        return view;
    }

}
