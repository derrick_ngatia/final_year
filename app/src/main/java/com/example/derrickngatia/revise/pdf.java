package com.example.derrickngatia.revise;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class pdf extends Fragment {
RecyclerView recyclerView;
ArrayList<document> documents;
static pdfAdapter adapter;
DatabaseReference databaseReference;
    public pdf() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view= inflater.inflate(R.layout.fragment_pdf, container, false);
        final TextView emptyView=(TextView)view.findViewById(R.id.empty_view);

        recyclerView=(RecyclerView)view.findViewById(R.id.documents);
        final String path=getArguments().getString("path");
        final String course=getArguments().getString("course");
        String year=getArguments().getString("year");
        String sem=getArguments().getString("sem");
        documents=new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("allcourses").child(course).child(year).child(sem).child(path).child("pdf");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                documents.clear();
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    document d=snapshot.getValue(document.class);
                    documents.add(d);
                }
                adapter=new pdfAdapter(documents,view.getContext());
                LinearLayoutManager manager=new LinearLayoutManager(view.getContext());
                recyclerView.setLayoutManager(manager);
                recyclerView.setAdapter(adapter);
                if (documents.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                }
                else {
                    recyclerView.setVisibility(View.VISIBLE);
                    emptyView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return view;
    }

}
