package com.example.derrickngatia.revise;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class year1 extends Fragment {

Button b1,b2,b3,b4;
    public year1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_year1, container, false);
        final String course=getArguments().getString("course");
        Toast.makeText(getActivity(),""+course,Toast.LENGTH_SHORT).show();
        b1=(Button)view.findViewById(R.id.yr1);
        b2=(Button)view.findViewById(R.id.yr2);
        b3=(Button)view.findViewById(R.id.yr3);
        b4=(Button)view.findViewById(R.id.yr4);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              sem s=new sem();
                Bundle b1=new Bundle();
                b1.putString("course",course);
                b1.putString("year","1");
              s.setArguments(b1);
              getFragmentManager().beginTransaction().replace(R.id.container89,s).addToBackStack("").commit();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sem s=new sem();
                Bundle b1=new Bundle();
                b1.putString("course",course);
                b1.putString("year","2");
                s.setArguments(b1);
                getFragmentManager().beginTransaction().replace(R.id.container89,s).addToBackStack("").commit();
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sem s=new sem();
                Bundle b1=new Bundle();
                b1.putString("course",course);
                b1.putString("year","3");
                s.setArguments(b1);
                getFragmentManager().beginTransaction().replace(R.id.container89,s).addToBackStack("").commit();
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sem s=new sem();
                Bundle b1=new Bundle();
                b1.putString("course",course);
                b1.putString("year","4");
                s.setArguments(b1);
                getFragmentManager().beginTransaction().replace(R.id.container89,s).addToBackStack("").commit();
            }
        });
        return view;
    }

}
