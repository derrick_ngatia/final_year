package com.example.derrickngatia.revise;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FeedBack extends Fragment {
  RecyclerView recyclerView;
  ProgressDialog
    dialog;
  ArrayList<ResponseFeedBack> arrayList;

    public FeedBack() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view=inflater.inflate(R.layout.fragment_feed_back, container, false);
        arrayList=new ArrayList<>();
        dialog=new ProgressDialog(view.getContext());
        dialog.setMessage("Retrieving ....");
        dialog.show();
        recyclerView=(RecyclerView)view.findViewById(R.id.feeds);
        FirebaseDatabase.getInstance().getReference().child("feedback").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                arrayList.clear();
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    ResponseFeedBack feedBack=snapshot.getValue(ResponseFeedBack.class);
                    arrayList.add(feedBack);
                }
                LinearLayoutManager gridLayoutManager=new LinearLayoutManager(view.getContext());
                recyclerView.setLayoutManager(gridLayoutManager);
                FeedBackAdapter adapter=new FeedBackAdapter(arrayList,view.getContext());
                recyclerView.setAdapter(adapter);
                dialog.dismiss();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return view;
    }

}
