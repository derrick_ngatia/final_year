package com.example.derrickngatia.revise;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

public class AboutUs extends AppCompatActivity {
    String Storage_Path = "All_Uploads/";
    String Database_Path = "All_Image_Uploads_Database";
    private static final int IMAGE_PICK_REQUEST =1234 ;
    Spinner mspinner1,myspinner2,myspinner3;
    EditText ed6;
    ImageView myimage;
    ProgressDialog progress;
    Button submit,choose;
    public Uri filepath;
    private StorageReference mStorageRef;
    FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
    DatabaseReference reference;
    DatabaseReference rootreference=firebaseDatabase.getReference();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        /*
        myimage=(ImageView)findViewById(R.id.myimage);
        mspinner1=(Spinner)findViewById(R.id.mysp1);
        myspinner2=(Spinner)findViewById(R.id.mysp2);
        myspinner3=(Spinner)findViewById(R.id.mysp3);
        ed6=(EditText)findViewById(R.id.imageName);
        progress=new ProgressDialog(this);
        progress.setTitle("uploading file");
        submit=(Button)findViewById(R.id.submit);
        choose=(Button)findViewById(R.id.choose);
        //spinner();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog(true,10000);
                if( mspinner1.getSelectedItem().equals("") | myspinner2.getSelectedItem().equals("") | myspinner3.getSelectedItem().equals("")){
                    Toast.makeText(getApplicationContext(),"Empty field found",Toast.LENGTH_LONG).show();
                }else {
                    upload();
                }
            }
        });
        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent,"select an image"),IMAGE_PICK_REQUEST);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==IMAGE_PICK_REQUEST && resultCode==RESULT_OK && data!=null && data.getData()!=null){
            filepath=data.getData();
            try {
                Bitmap map= MediaStore.Images.Media.getBitmap(getContentResolver(),filepath);
                myimage.setImageBitmap(map);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    public  void upload(){
        mStorageRef = FirebaseStorage.getInstance().getReference();
        StorageReference riversRef=mStorageRef.child(""+myspinner3.getSelectedItem().toString()+"/"+ed6.getText().toString()+"."+GetFileExtension(filepath));
        riversRef.putFile(filepath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                showProgressDialog(true,10000);
                String currentDateTimeString = DateFormat.getDateInstance().format(new Date());
                Toast.makeText(getApplicationContext(),"file uploaded",Toast.LENGTH_LONG).show();
                reference = rootreference.child(mspinner1.getSelectedItem().toString());
                user myuser = new user();
                myuser.setUnit_name(ed2.getText().toString());
                myuser.setUnit_code(ed3.getText().toString());
                myuser.setYear_sem(ed4.getText().toString() + "_" + ed5.getText().toString());
                reference.push().setValue(myuser);
                String TempImageName=ed6.getText().toString();
                DatabaseReference image_info_reference=rootreference.child(ed1.getText().toString()).child(ed2.getText().toString());
                upload upload=new upload(TempImageName,taskSnapshot.getDownloadUrl().toString(),currentDateTimeString,"0");
                image_info_reference.push().setValue(upload);
                progress.dismiss();

            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double mprogress=(100.0 *taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                progress.setMessage((int) mprogress+"%uploaded...");
            }
        });
        */
    }
    private void showProgressDialog(boolean show, long time) {
        try {
            if (progress != null) {
                if (show) {
                    progress.show();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            if(progress!=null && progress.isShowing()) {
                                progress.dismiss();
                            }
                        }
                    }, time);
                } else {
                    progress.dismiss();
                }
            }
        }catch(IllegalArgumentException e){
        }catch(Exception e){
        }
    }
    public String GetFileExtension(Uri uri) {

        ContentResolver contentResolver = getContentResolver();

        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

        // Returning the file Extension.
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri)) ;

    }
    /*
    public void spinner(){
        rootreference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> mylist = new ArrayList<String>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    showProgressDialog(false,0);
                    String myvalue = postSnapshot.getKey();
                    mylist.add(myvalue);
                }
               ArrayAdapter<String> adapter=new ArrayAdapter<String>(MainActivity.this,R.layout.support_simple_spinner_dropdown_item,mylist);
                mspinner.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                showProgressDialog(false,0);
                Toast.makeText(getBaseContext(), "" + databaseError, Toast.LENGTH_SHORT).show();
            }


        });

    }
    */
}
