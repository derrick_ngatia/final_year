package com.example.derrickngatia.revise;

import java.util.Date;

/**
 * Created by DERRICK NGATIA on 10/15/2017.
 */

public class upload {

    private String imageName;
    private String imageURL;
    private Long TimeOfupload;
    private String views;
    private String year_of_examination;


    public upload() {
    }

    public upload(String imageName, String imageURL, String views, String year_of_examination) {
        this.imageName = imageName;
        this.imageURL = imageURL;
        this.views = views;
        this.year_of_examination = year_of_examination;
        this.TimeOfupload=new Date().getTime();
    }


    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Long getTimeOfupload() {
        return TimeOfupload;
    }

    public void setTimeOfupload(Long timeOfupload) {
        TimeOfupload = timeOfupload;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getYear_of_examination() {
        return year_of_examination;
    }

    public void setYear_of_examination(String year_of_examination) {
        this.year_of_examination = year_of_examination;
    }
}
