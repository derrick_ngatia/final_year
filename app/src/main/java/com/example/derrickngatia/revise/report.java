package com.example.derrickngatia.revise;

/**
 * Created by DERRICK NGATIA on 6/17/2018.
 */

public class report {
    private String imageUrl;
    private String imageName;
    private String imageTime;
    private String description;

    public report() {
    }

    public report(String imageUrl, String imageName, String imageTime, String description) {
        this.imageUrl = imageUrl;
        this.imageName = imageName;
        this.imageTime = imageTime;
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageTime() {
        return imageTime;
    }

    public void setImageTime(String imageTime) {
        this.imageTime = imageTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
